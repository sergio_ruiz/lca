#version 130
#extension GL_ARB_uniform_buffer_object: enable
#extension GL_EXT_gpu_shader4: enable
#extension GL_ARB_draw_instanced: enable

#define DEG2RAD			0.01745329251994329576

attribute vec2			texCoord0;
attribute vec3			normal;

varying vec3			lightVec;
varying vec3			N;
varying vec2			loDet;

uniform samplerBuffer	posTextureBuffer;
uniform samplerBuffer	idsTextureBuffer;
uniform mat4			ViewMat4x4;
uniform vec3			camPos;

mat4 cenital = mat4( 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 );
mat4 azimuth = mat4( 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 );

//=======================================================================================
mat4 rotationToMatrix( vec2 rotation )
{
	float sinC = sin( rotation.x );
	float cosC = cos( rotation.x );
	float sinA = sin( rotation.y );
	float cosA = cos( rotation.y );
	cenital[1][1] =  cosC;
	cenital[1][2] =  sinC;
	cenital[2][1] = -sinC;
	cenital[2][2] =  cosC;
	azimuth[0][0] =  cosA;
	azimuth[0][2] = -sinA;
	azimuth[2][0] =  sinA;
	azimuth[2][2] =  cosA;
	return azimuth * cenital;
}
//=======================================================================================
void main( void )
{
	gl_TexCoord[ 0 ].st		= texCoord0.xy;

	// ID, GROUP_ID, W, LOD
	vec4 ids				= texelFetchBuffer( idsTextureBuffer, gl_InstanceID );
	loDet					= vec2( ids.w );
	float agent_id			= ids.x;
	vec4 inputv				= texelFetchBuffer( posTextureBuffer, int(agent_id) );
	vec4 position			= vec4( inputv.x, 0.0, inputv.z, 1.0 );
	vec2 rotation			= vec2( 0.0, inputv.w );
	
	mat4 transMat4x4		= rotationToMatrix( rotation );
	transMat4x4[ 3 ][ 0 ]	= position.x;
	transMat4x4[ 3 ][ 1 ]	= position.y;
	transMat4x4[ 3 ][ 2 ]	= position.z;
	mat4 modelViewMat		= ViewMat4x4 * transMat4x4;
	mat3 myNormalMat		= mat3( ViewMat4x4[0], ViewMat4x4[1], ViewMat4x4[2] ) * gl_NormalMatrix;
	vec4 vertex = gl_Vertex;

	//vertex.y += agent_id;
	vec4 P					= (modelViewMat * vertex);
	lightVec				= normalize( camPos - P.xyz );
	P						= P/P.w;
	gl_Position				= gl_ProjectionMatrix * P;
	N						= normalize(gl_NormalMatrix * normal);
}
