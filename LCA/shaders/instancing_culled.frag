
varying vec2	loDet;
varying vec3	lightVec;
varying vec3	N;

uniform sampler2D diffuseTexture;
uniform float showLod;

void main( void )
{
	vec4 diffuseMaterial = texture2D( diffuseTexture, gl_TexCoord[0].st );
	if( showLod > 0.0f )
	{
		if( loDet.x > 0.0f && loDet.x <= 1.0f )
		{
			gl_FragColor = diffuseMaterial * vec4( 0.8f, 0.1f, 0.1f, 1.0f );
		}
		else if( loDet.x <= 2.0f )
		{
			gl_FragColor = diffuseMaterial * vec4( 0.1f, 0.8f, 0.1f, 1.0f );
		}
		else if( loDet.x <= 3.0f )
		{
			gl_FragColor = diffuseMaterial * vec4( 0.1f, 0.1f, 0.8f, 1.0f );
		}
		else if( loDet.x <= 4.0f )
		{
			gl_FragColor = diffuseMaterial * vec4( 0.8f, 0.1f, 0.8f, 1.0f );
		}
		else
		{
			gl_FragColor = vec4( 0.0f, 0.0f, 0.0f, 1.0f );
		}
	}
	else
	{
		gl_FragColor = diffuseMaterial * 0.12;
		float NdotL; //cosin normal lightDir
		NdotL = max( dot( lightVec, N ), 0.12 );
		lightVec *= vec3( -1 );
		float MNdotL = max( dot( lightVec, N ), 0.12 );
		vec4 diffuseC = diffuseMaterial * (NdotL + MNdotL);
		gl_FragColor += diffuseC;
		gl_FragColor.a = 1.0;
	}
}
