// @REV_TAG SRL_04_2011

#pragma once
#include "cLogManager.h"

#if defined(_WIN32)
	#include <windows.h>
#elif defined(_UNIX)
	// MISSING DECLARATION
#elif defined(_MAC)
	// MISSING DECLARATION
#endif

//=======================================================================================

LogManager::LogManager( string& log_filename )
{
	filename						= string( log_filename );
	sections_map[ERROR]				= string( "___ERROR" );
	sections_map[GL_ERROR]			= string( "__GL_ERR" );
	sections_map[WARNING]			= string( "_WARNING" );
	sections_map[CONFIGURATION]		= string( "__CONFIG" );
	sections_map[XML]				= string( "_____XML" );
	sections_map[CONTEXT]			= string( "_CONTEXT" );
	sections_map[GLSL_MANAGER]		= string( "____GLSL" );
	sections_map[FBO_MANAGER]		= string( "_____FBO" );
	sections_map[VBO_MANAGER]		= string( "_____VBO" );
	sections_map[EXTENSION_MANAGER]	= string( "_EXT_MAN" );
	sections_map[TEXTURE_MANAGER]	= string( "_TEXTURE" );
	sections_map[CROWD_MANAGER]		= string( "___CROWD" );
	sections_map[OBSTACLE_MANAGER]	= string( "OBSTACLE" );
	sections_map[BOUNDING_VOLUME]	= string( "_BOUND_V" );
	sections_map[MODEL]				= string( "___MODEL" );
	sections_map[GPU_RAM]			= string( "_GPU_RAM" );
	sections_map[ALUT]				= string( "____ALUT" );
	sections_map[FMOD]				= string( "____FMOD" );
	sections_map[OGG]				= string( "_____OGG" );
	sections_map[MD2]				= string( "_____MD2" );
	sections_map[SKYBOX]			= string( "__SKYBOX" );
	sections_map[INFORMATION]		= string( "____INFO" );
	sections_map[STATUS]			= string( "__STATUS" );
	sections_map[STATISTICS]		= string( "___STATS" );
	sections_map[CLEANUP]			= string( "_CLEANUP" );
	sections_map[NET]				= string( "_____NET" );
	sections_map[EXIT]				= string( "____EXIT" );
	sections_map[STATIC_LOD]		= string( "___S_LOD" );
	sections_map[CUDA]				= string( "____CUDA" );
	
	html_colors_map[ERROR]				= string( "#FF0000" );
	html_colors_map[GL_ERROR]			= string( "#FF0000" );
	html_colors_map[WARNING]			= string( "#FF8000" );
	html_colors_map[CONFIGURATION]		= string( "#610B38" );
	html_colors_map[XML]				= string( "#380B61" );
	html_colors_map[CONTEXT]			= string( "#0404B4" );
	html_colors_map[GLSL_MANAGER]		= string( "#5E9D32" );
	html_colors_map[FBO_MANAGER]		= string( "#5FB404" );
	html_colors_map[VBO_MANAGER]		= string( "#95D17B" );
	html_colors_map[EXTENSION_MANAGER]	= string( "#AEB404" );
	html_colors_map[TEXTURE_MANAGER]	= string( "#DF7401" );
	html_colors_map[CROWD_MANAGER]		= string( "#2244DA" );
	html_colors_map[OBSTACLE_MANAGER]	= string( "#6543DC" );
	html_colors_map[BOUNDING_VOLUME]	= string( "#9A2EFE" );
	html_colors_map[MODEL]				= string( "#2E2EFE" );
	html_colors_map[GPU_RAM]			= string( "#2E9AFE" );
	html_colors_map[ALUT]				= string( "#58FAAC" );
	html_colors_map[FMOD]				= string( "#58FA58" );
	html_colors_map[OGG]				= string( "#393B0B" );
	html_colors_map[MD2]				= string( "#F78181" );
	html_colors_map[SKYBOX]				= string( "#0B6138" );
	html_colors_map[INFORMATION]		= string( "#000000" );
	html_colors_map[STATUS]				= string( "#000000" );
	html_colors_map[STATISTICS]			= string( "#888888" );
	html_colors_map[CLEANUP]			= string( "#FF0000" );
	html_colors_map[NET]				= string( "#5AA250" );
	html_colors_map[EXIT]				= string( "#FF0000" );
	html_colors_map[STATIC_LOD]			= string( "#385B66" );
	html_colors_map[CUDA]				= string( "#009900" );

	char timeb[128];
	_strtime_s( timeb, 128 );
	printf( "SRL-BHA::CASIM_Init@%s\n", timeb );
	cout << "Log File: " << filename.c_str() << "\n";
	logFile.open( filename.c_str(), ios::trunc );
	logFile << "<html>\n";
	logFile << "<table border=\"1\">\n";
	logFile << "<tr>\n";
	logFile << "<td colspan=\"3\" align=\"center\">\n";
	logFile << "<b>SRL-BHA::CASIM_Init@<span style=\"color: #0000FF;\">";
	logFile << timeb << "</span></b>\n";
	logFile << "</td>\n";
	logFile << "</tr>\n";
	separator();
}

//=======================================================================================

LogManager::~LogManager( void )
{
	logFile << "</table>\n";
	logFile << "</html>\n";
	filename.erase();
	sections_map.clear();
	logFile.close();
}

//=======================================================================================

void LogManager::log( int section, string format, string data )
{
	const char *fmt = format.c_str();
	const char *dat = data.c_str();
	log( section, fmt, dat );
}

//=======================================================================================

void LogManager::log( int section, string data )
{
	const char *dat = data.c_str();
	log( section, dat );
}

//=======================================================================================

void LogManager::log( int section, const char* format, const char* data )
{
	va_list args;
	char buffer[1024];
	getTime();
	cout << timeBuf << " | " << sections_map[section].c_str() << " | ";
	printf( format, data );
	cout << endl;
	va_start( args, format );
	vsprintf_s( buffer, 1024, format, args );
	va_end( args );

	logFile << "<tr>\n";
	logFile << "<td><span style=\"color: #0000FF;\">" << timeBuf << "</span></td><td>";
	logFile << "<span style=\"color: " << html_colors_map[section].c_str() << "\">";
	logFile << sections_map[section].c_str() << "</span></td><td>";
	logFile << buffer << "</td>\n";
	logFile << "</tr>\n";
}

//=======================================================================================

void LogManager::log( int section, const char* format, ... )
{
    va_list args;
    char buffer[8192];

	getTime();
	cout << timeBuf << " | " << sections_map[section].c_str() << " | ";
	
    va_start( args, format );
	//perror( buffer );
	vsprintf_s( buffer, 8192, format, args );
    va_end( args );

	cout << buffer << endl;

	string sbuffert( buffer );
	string sbuffer;
	for( unsigned int b = 0; b < sbuffert.length(); b++ )
	{
		if( sbuffert[b] == '\n' )
		{
			sbuffer.append( string("<br>") );
		}
		else
		{
			sbuffer.push_back( sbuffert[b] );
		}
	}
	
	logFile << "<tr>\n";
	logFile << "<td><span style=\"color: #0000FF;\">" << timeBuf << "</span></td><td>";
	logFile << "<span style=\"color: " << html_colors_map[section].c_str() << "\">";
	logFile << sections_map[section].c_str() << "</span></td><td>";
	logFile << sbuffer.c_str() << "</td>\n";
	logFile << "</tr>\n";
}

//=======================================================================================

void LogManager::separator( void )
{
	file_separator();
	console_separator();
}

//=======================================================================================

void LogManager::console_log( int section, string format, string data )
{
	const char *fmt = format.c_str();
	const char *dat = data.c_str();
	console_log( section, fmt, dat );
}

//=======================================================================================

void LogManager::console_log( int section, string data )
{
	const char *dat = data.c_str();
	console_log( section, dat );
}

//=======================================================================================

void LogManager::console_log( int section, const char* format, const char* data )
{
	va_list args;
	char buffer[1024];
	getTime();
	cout << timeBuf << " | " << sections_map[section].c_str() << " | ";
	printf( format, data );
	cout << endl;
	va_start( args, format );
	vsprintf_s( buffer, 1024, format, args );
	va_end( args );
}

//=======================================================================================

void LogManager::console_log( int section, const char* format, ... )
{
    va_list args;
    char buffer[1024];
	getTime();
	cout << timeBuf << " | " << sections_map[section].c_str() << " | ";
    va_start( args, format );
	vsprintf_s( buffer, 1024, format, args );
    va_end( args );
	cout << buffer << endl;
}

//=======================================================================================

void LogManager::file_log( int section, string format, string data )
{
	const char *fmt = format.c_str();
	const char *dat = data.c_str();
	file_log( section, fmt, dat );
}

//=======================================================================================

void LogManager::file_log( int section, string data )
{
	const char *dat = data.c_str();
	file_log( section, dat );
}

//=======================================================================================

void LogManager::file_log( int section, const char* format, const char* data )
{
	va_list args;
	char buffer[1024];
	getTime();
	va_start( args, format );
	vsprintf_s( buffer, 1024, format, args );
	va_end( args );

	logFile << "<tr>\n";
	logFile << "<td><span style=\"color: #0000FF;\">" << timeBuf << "</span></td><td>";
	logFile << "<span style=\"color: " << html_colors_map[section].c_str() << "\">";
	logFile << sections_map[section].c_str() << "</span></td><td>";
	logFile << buffer << "</td>\n";
	logFile << "</tr>\n";
}

//=======================================================================================

void LogManager::file_log( int section, const char* format, ... )
{
    va_list args;
    char buffer[1024];
	getTime();
    va_start( args, format );
	vsprintf_s( buffer, 1024, format, args );
    va_end( args );

	logFile << "<tr>\n";
	logFile << "<td><span style=\"color: #0000FF;\">" << timeBuf << "</span></td><td>";
	logFile << "<span style=\"color: " << html_colors_map[section].c_str() << "\">";
	logFile << sections_map[section].c_str() << "</span></td><td>";
	logFile << buffer << "</td>\n";
	logFile << "</tr>\n";
}

//=======================================================================================

void LogManager::console_separator( void )
{
	cout << "-------------+----------+-----------------------------";
	cout << "-------------------------" << endl;
}

//=======================================================================================

void LogManager::file_separator( void )
{
	logFile << "<tr><td align=\"center\"><span style=\"color: #CCCCCC\">TIME</span>";
	logFile << "</td><td align=\"center\"><span style=\"color: #CCCCCC\">";
	logFile << "SECTION</span></td>";
	logFile << "<td align=\"center\"><span style=\"color: #CCCCCC\">";
	logFile << "MESSAGE</span></td></tr>\n";
}

//=======================================================================================

void LogManager::getTime( void )
{
#if defined(_WIN32)
	SYSTEMTIME st;
    GetLocalTime( &st );
	printTime( "%02d:%02d:%02d:%05d", 
			   st.wHour, 
			   st.wMinute, 
			   st.wSecond, 
			   st.wMilliseconds );
#elif defined(_UNIX)
	// MISSING DECLARATION
#elif defined(_MAC)
	// MISSING DECLARATION
#endif
}

//=======================================================================================

void LogManager::printTime( const char* format, ... )
{
	va_list args;
    va_start( args, format );
	vsprintf_s( timeBuf, 20, format, args );
    va_end( args );
}

//=======================================================================================

void LogManager::console_prc( int curr_val, int max_val )
{
	float full = (float)max_val;
	float prc  = (float)curr_val * 100.0f / full;
	cout.precision( 4 );
	if( prc <= 10.0f )
	{
		cout << "\r";
		cout << "[|         ] " << prc << "% ";
	}
	else if( prc <= 20.0f )
	{
		cout << "\r";
		cout << "[||        ] " << prc << "% ";
	}
	else if( prc <= 30.0f )
	{
		cout << "\r";
		cout << "[|||       ] " << prc << "% ";
	}
	else if( prc <= 40.0f )
	{
		cout << "\r";
		cout << "[||||      ] " << prc << "% ";
	}
	else if( prc <= 50.0f )
	{
		cout << "\r";
		cout << "[|||||     ] " << prc << "% ";
	}
	else if( prc <= 60.0f )
	{
		cout << "\r";
		cout << "[||||||    ] " << prc << "% ";
	}
	else if( prc <= 70.0f )
	{
		cout << "\r";
		cout << "[|||||||   ] " << prc << "% ";
	}
	else if( prc <= 80.0f )
	{
		cout << "\r";
		cout << "[||||||||  ] " << prc << "% ";
	}
	else if( prc <= 90.0f )
	{
		cout << "\r";
		cout << "[||||||||| ] " << prc << "% ";
	}
	else
	{
		cout << "\r";
		cout << "[||||||||||] " << prc << "% ";
	}
}

//=======================================================================================

void LogManager::logStatistics( unsigned int tex_w, 
								unsigned int vert_c, 
								unsigned int frame_vert_c,
								unsigned int vert_size
							  )
{
	log( STATISTICS, 
		 "Textures weight: %i MB", 
		 BYTE2MB( tex_w ) );
	log( STATISTICS, 
		 "Vertices in scene: %i K (%i MB)", 
		 vert_c / 1000,
		 BYTE2MB( (vert_c * vert_size) ) );
	log( STATISTICS, 
		 "Vertices per frame: %i K (%i MB)", 
		 frame_vert_c / 1000, 
		 BYTE2MB( (frame_vert_c * vert_size) ) );
	log( STATISTICS, 
		 "Triangles per frame: %i K (%i MB)", 
		 frame_vert_c / 3000, 
		 BYTE2MB( (frame_vert_c * vert_size) ) );
}

//=======================================================================================

void LogManager::logPeakFps( float			peakFps, 
							 float			avgFps, 
							 unsigned long	frame_c,
							 float			spf,
							 unsigned int	culled,
							 unsigned int	total
						   )
{
	log( STATISTICS, 
		 "New PEAK FPS: %.2f. Average FPS: %.2f. Frame: %u.",
		 peakFps,
		 avgFps,
		 frame_c );
	log( STATISTICS,
		"Seconds per frame: %.3f. Culled objects: %i (%i).",
		 spf,
		 culled,
		 total );
}

//=======================================================================================

void LogManager::logLowFps( float			lowFps,  
						    float			avgFps, 
							unsigned long	frame_c,
							float			spf,
							unsigned int	culled,
							unsigned int	total
						  )
{
	log( STATISTICS, 
		 "New LOW  FPS: %.2f. Average FPS: %.2f. Frame: %u.",
		 lowFps,
		 avgFps,
		 frame_c );
	log( STATISTICS,
		"Seconds per frame: %.3f. Culled objects: %i.",
		 spf,
		 culled,
		 total );
}

//=======================================================================================

void LogManager::file_prc( int section, int curr_val, int max_val )
{
	getTime();
	float full = (float)max_val;
	float prc  = (float)curr_val * 100.0f / full;
	logFile << "<tr>\n";
	logFile << "<td><span style=\"color: #0000FF;\">" << timeBuf << "</span></td><td>";
	logFile << "<span style=\"color: " << html_colors_map[section].c_str() << "\">";
	logFile << sections_map[section].c_str() << "</span></td><td>";

	if( prc <= 10.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| | | | | | | | |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 20.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| | | | | | | |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 30.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| | | | | | |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 40.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| | | | |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 50.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| | | | |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 60.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| | | |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 70.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F62817;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| | |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 80.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F62817;\">| </span>";
		logFile << "<span style=\"color: #F62817;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| |</span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else if( prc <= 90.0f )
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F62817;\">| </span>";
		logFile << "<span style=\"color: #F62817;\">| </span>";
		logFile << "<span style=\"color: #FF0000;\">| </span>";
		logFile << "<span style=\"color: #DDDDDD;\">| </span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	else
	{
		logFile << "[";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #00FF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #FFFF00;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F88017;\">| </span>";
		logFile << "<span style=\"color: #F62817;\">| </span>";
		logFile << "<span style=\"color: #F62817;\">| </span>";
		logFile << "<span style=\"color: #FF0000;\">| </span>";
		logFile << "<span style=\"color: #FF0000;\">| </span>";
		logFile << "] ";
		logFile.precision( 4 );
		logFile << prc << "% ";
	}
	logFile << "</td></tr>";
}

//=======================================================================================
