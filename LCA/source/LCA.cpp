// @REV_TAG SRL_10_2013

#include "cMacros.h"

#define CAM_PERSP				2
#define INSTANCING_OFF			0
#define INSTANCING_ON			1

#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <string>

#include "glew.h"
#include "freeglut.h"
#include "cTimer.h"

#include "glm.hpp"

#include "cGlslManager.h"
#include "cVboManager.h"
#include "cFboManager.h"
#include "cTextureManager.h"
#include "cCamera.h"
#include "cVertex.h"
#include "cAxes.h"
#include "cLogManager.h"
#include "cModel3D.h"

#include "cObstacleManager.h"
#include "cLCACudaPathManager.h"

using namespace std;

void cleanup( void );
void initTexCoords( void );

unsigned int	INSTANCING			= INSTANCING_ON;
//float			PLANE_SCALE			= 20000.0f;
float			PLANE_SCALE			= 5000.0f;

float			fps					= 0.0f;
float			delta_time			= 0.0f;
float			prev_time			= 0.0f;
float			camAccel			= 10.0f;
float			path_param			= 0.0f;
float			timeCont			= 0.0f;

unsigned long	frame_cont			= 0;
int				update_frame		= 0;

bool			rotatecam			= false;
bool			runpaths			= false;
bool			agentcam			= false;
bool			showstats			= false;
bool			animating			= false;
bool			wireframe			= false;
bool			hideScenario		= true;
bool			hideCharacters		= false;
bool			ctrlDown			= false;

unsigned int	pos_tex_coords		= 0;
unsigned int	pos_tex_size		= 0;
unsigned int	nut_tex				= 0;
unsigned int	floor_tex			= 0;

unsigned int	VIEWPORT_WIDTH		= INIT_WINDOW_WIDTH;
unsigned int	VIEWPORT_HEIGHT		= INIT_WINDOW_HEIGHT;

vector<unsigned int>	d_lod;

VboManager*			vbo_manager				= NULL;
GlslManager*		glsl_manager			= NULL;
FboManager*			fbo_manager				= NULL;
GlErrorManager*		err_manager				= NULL;
LogManager*			log_manager				= NULL;
Camera*				camera					= NULL;
Axes*				axes					= NULL;

vector<Model3D*>	nut;
Model3D*			floorm					= NULL;

ObstacleManager*	obstacle_manager		= NULL;
LCACudaPathManager*	lca_path_manager		= NULL;

StaticLod*			static_lod				= NULL;
sVBOLod				vbo_lod[NUM_LOD];

string				title					= "SRL::RVO@GPU v1.0";
string				fps_string				= "FPS:        ";
string				delta_time_string		= "DELTA TIME: ";
string				culled_string			= "CULLED:     ";
string				lod1_string				= "LOD1:       ";
string				lod2_string				= "LOD2:       ";
string				lod3_string				= "LOD3:       ";
string				lod4_string				= "LOD4:       ";

bool				lMouseDown				= false;
bool				rMouseDown				= false;
bool				mMouseDown				= false;

vector<glm::vec3>	predef_cam_pos;
vector<glm::vec3>	predef_cam_dir;
vector<glm::vec3>	predef_cam_up;
unsigned int		predef_cam_index		= 0;

//
//=======================================================================================
//
//TO-DO: create an XML parser to read more lights.
void init_lights( void )
{
	unsigned int num_lights = 2;
	float global_ambient[]	= {    0.2f,	  0.2f,		0.2f,	1.0f };

	float position1[]		= {    PLANE_SCALE,	PLANE_SCALE*2.0f, -PLANE_SCALE,	1.0f };
	float diffuse1[]		= {    1.0f,	  1.0f,		1.0f,	1.0f };
	float specular1[]		= {    0.4f,	  0.4f,		0.4f,	1.0f };
	float ambient1[]		= {    0.2f,	  0.2f,		0.2f,	1.0f };

	float position2[]		= {   20.0f,    100.0f,	  500.0f,	0.0f };
	float diffuse2[]		= {    1.0f,	  1.0f,		1.0f,	1.0f };
	float specular2[]		= {    0.4f,	  0.4f,		0.4f,	1.0f };
	float ambient2[]		= {    0.2f,	  0.2f,		0.2f,	1.0f };

	float* position3;
	position3				= new float[4];
	position3[0]			=  2000.0f;
	position3[1]			=   200.0f;
	position3[2]			=  2000.0f;
	position3[3]			=     1.0f;
	//float position3[]		= {    0.0f,      0.0f,	    1.0f,	0.0f };
	float diffuse3[]		= {    1.0f,	  1.0f,		1.0f,	1.0f };
	float specular3[]		= {    0.4f,	  0.4f,		0.4f,	1.0f };
	float ambient3[]		= {    0.2f,	  0.2f,		0.2f,	1.0f };

	float* position4;
	position4				= new float[4];
	position3[0]			=  -2000.0f;
	position3[1]			=    200.0f;
	position3[2]			=   2000.0f;
	position3[3]			=      1.0f;

	//float position4[]		= {    0.0f,     -1.0f,	    1.0f,	0.0f };
	float diffuse4[]		= {    1.0f,	  1.0f,		1.0f,	1.0f };
	float specular4[]		= {    0.4f,	  0.4f,		0.4f,	1.0f };
	float ambient4[]		= {    0.2f,	  0.2f,		0.2f,	1.0f };


	glLightModelfv( GL_LIGHT_MODEL_AMBIENT, global_ambient );
	glLightModeli( GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR );
/*
	Source: http://www.csit.parkland.edu/~dbock/Portfolio/Content/Lights.html
	GL_LIGHT_MODEL_LOCAL_VIEWER:
	GL_FALSE: infinite viewpoint - viewing vector doesn't change across surface - faster, 
			  less realistic
    GL_TRUE:  local viewpoint - viewing vector changes across surface - slower, 
			  more realistic 
*/
	glLightModeli( GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE );

	glLightfv( GL_LIGHT0, 
			   GL_POSITION, 
			   position1	);
	glLightfv( GL_LIGHT0, 
			   GL_DIFFUSE,  
			   diffuse1		);
	glLightfv( GL_LIGHT0, 
			   GL_SPECULAR, 
			   specular1	);
	glLightfv( GL_LIGHT0, 
			   GL_AMBIENT,  
			   ambient1		);
	glEnable( GL_LIGHT0		);

	glLightfv( GL_LIGHT1, 
			   GL_POSITION, 
			   position2	);
	glLightfv( GL_LIGHT1, 
			   GL_DIFFUSE,  
			   diffuse2		);
	glLightfv( GL_LIGHT1, 
			   GL_SPECULAR, 
			   specular2	);
	glLightfv( GL_LIGHT1, 
			   GL_AMBIENT,  
			   ambient2		);
	glEnable( GL_LIGHT1		);

	glLightfv( GL_LIGHT2, 
			   GL_POSITION, 
			   position3	);
	glLightfv( GL_LIGHT2, 
			   GL_DIFFUSE,  
			   diffuse3		);
	glLightfv( GL_LIGHT2, 
			   GL_SPECULAR, 
			   specular3	);
	glLightfv( GL_LIGHT2, 
			   GL_AMBIENT,  
			   ambient3		);
	glEnable( GL_LIGHT2		);

	glLightfv( GL_LIGHT3, 
			   GL_POSITION, 
			   position4	);
	glLightfv( GL_LIGHT3, 
			   GL_DIFFUSE,  
			   diffuse4		);
	glLightfv( GL_LIGHT3, 
			   GL_SPECULAR, 
			   specular4	);
	glLightfv( GL_LIGHT3, 
			   GL_AMBIENT,  
			   ambient4		);
	glEnable( GL_LIGHT3		);

	glEnable( GL_LIGHTING	);

}
//
//=======================================================================================
//
void reshape( int w, int h )
{
	glViewport( 0, 0, (GLsizei) w, (GLsizei) h );
	VIEWPORT_WIDTH = (unsigned int)w;
	VIEWPORT_HEIGHT = (unsigned int)h;
	if( !fbo_manager->updateFboDims( "display_fbo", (unsigned int) w, (unsigned int) h ) )
	{
		system( "pause" );
		cleanup();
		exit( 1 );
	}
	else
	{
		printf( "Reshaped to %ix%i.\n", w, h );
	}
	camera->setView();
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	gluPerspective( camera->getFrustum()->getFovY(), 
					camera->getFrustum()->RATIO, 
					camera->getFrustum()->getNearD(), 
					camera->getFrustum()->getFarD() );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	glsl_manager->activate( "vfc_lod_assignment" );
	{
		glsl_manager->setUniformf( "vfc_lod_assignment", "tang",      camera->getFrustum()->TANG		);
		glsl_manager->setUniformf( "vfc_lod_assignment", "farPlane",  camera->getFrustum()->getFarD()	);
		glsl_manager->setUniformf( "vfc_lod_assignment", "nearPlane", camera->getFrustum()->getNearD()	);
		glsl_manager->setUniformf( "vfc_lod_assignment", "ratio",	  camera->getFrustum()->getRatio()	);
	}
	glsl_manager->deactivate( "vfc_lod_assignment" );
}
//
//=======================================================================================
//
void init( void )
{
	//glClearColor( 195.0f/255.0f, 195.0f/255.0f, 195.0f/255.0f, 0.0 );
	//glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
	glShadeModel( GL_SMOOTH );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_TEXTURE_2D );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	glEnable( GL_MULTISAMPLE );
	glEnable( GL_CULL_FACE );
	init_lights();


	glm::vec3 p0;
	glm::vec3 p1;
	glm::vec3 p2;
	glm::vec3 p3;
	glm::vec3 p4;

	glm::vec3 d0;
	glm::vec3 d1;
	glm::vec3 d2;
	glm::vec3 d3;
	glm::vec3 d4;

	glm::vec3 r0;
	glm::vec3 r1;
	glm::vec3 r2;
	glm::vec3 r3;
	glm::vec3 r4;

	glm::vec3 u0;
	glm::vec3 u1;
	glm::vec3 u2;
	glm::vec3 u3;
	glm::vec3 u4;

	p0.x = -PLANE_SCALE;
	p0.y =  500.0f;
	p0.z =  PLANE_SCALE;
	r0.x =  PLANE_SCALE;
	r0.y =  0.0f;
	r0.z =  PLANE_SCALE;

	p1.x =  PLANE_SCALE;
	p1.y =  500.0f;
	p1.z =  PLANE_SCALE;
	r1.x =  PLANE_SCALE;
	r1.y =  0.0f;
	r1.z = -PLANE_SCALE;

	p2.x =  PLANE_SCALE;
	p2.y =  500.0f;
	p2.z = -PLANE_SCALE;
	r2.x = -PLANE_SCALE;
	r2.y =  0.0f;
	r2.z = -PLANE_SCALE;

	p3.x = -PLANE_SCALE;
	p3.y =  500.0f;
	p3.z = -PLANE_SCALE;
	r3.x = -PLANE_SCALE;
	r3.y =  0.0f;
	r3.z =  PLANE_SCALE;

	p4.x =  0.0f;
	p4.y =  PLANE_SCALE*2.0f;
	p4.z =  0.0f;
	r4.x =  1.0f;
	r4.y =  0.0f;
	r4.z =  0.0f;

	d0 = -p0;
	d1 = -p1;
	d2 = -p2;
	d3 = -p3;
	d4 = -p4;

	d0 = glm::normalize( d0 );
	d1 = glm::normalize( d1 );
	d2 = glm::normalize( d2 );
	d3 = glm::normalize( d3 );
	d4 = glm::normalize( d4 );

	r0 = glm::normalize( r0 );
	r1 = glm::normalize( r1 );
	r2 = glm::normalize( r2 );
	r3 = glm::normalize( r3 );
	r4 = glm::normalize( r4 );

	u0 = glm::cross( r0, d0 );
	u1 = glm::cross( r1, d1 );
	u2 = glm::cross( r2, d2 );
	u3 = glm::cross( r3, d3 );
	u4 = glm::cross( r4, d4 );

	predef_cam_pos.push_back( p0 );
	predef_cam_pos.push_back( p1 );
	predef_cam_pos.push_back( p2 );
	predef_cam_pos.push_back( p3 );
	predef_cam_pos.push_back( p4 );

	predef_cam_dir.push_back( d0 );
	predef_cam_dir.push_back( d1 );
	predef_cam_dir.push_back( d2 );
	predef_cam_dir.push_back( d3 );
	predef_cam_dir.push_back( d4 );

	predef_cam_up.push_back( u0 );
	predef_cam_up.push_back( u1 );
	predef_cam_up.push_back( u2 );
	predef_cam_up.push_back( u3 );
	predef_cam_up.push_back( u4 );


	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	return;
}
//
//=======================================================================================
//
void displayText( int x, int y, char* txt ) 
{
	GLint viewportCoords[ 4 ];
	glColor3f( 0.0, 1.0, 0.0 );
	glGetIntegerv( GL_VIEWPORT, viewportCoords );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D( 0.0, viewportCoords[2], 0.0, viewportCoords[3] );
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();

	glRasterPos2i( x, viewportCoords[3]-y );

	while (*txt) {glutBitmapCharacter( GLUT_BITMAP_9_BY_15, *txt ); txt++;}
	glPopMatrix();
	glMatrixMode( GL_PROJECTION );	
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
}
//
//=======================================================================================
//
void idle( void )
{
	// UPDATE_VARS:
	Timer::getInstance()->update();
	fps = Timer::getInstance()->getFps();
	fps <= 1.0f ? delta_time = 1.0f / 30.0f : delta_time = 1.0f / fps;
	
	delta_time > 0 ? update_frame = frame_cont % int(delta_time * 1000.0) : update_frame; // every delta_time msecs, update frame
	frame_cont++;

	timeCont+=delta_time;

	if( runpaths )
	{
		if( timeCont > 1/60.0 )
		{
			lca_path_manager->runCuda( AGENTS_NPOT, AGENTS_NPOT, path_param );
			path_param += 0.01f;
			//if( path_param > 6.28318530718f )
			//{
			//	path_param = 0.0f;
			//}
			timeCont = 0;
		}
	}

	if( frame_cont % 30 == 0 ) // every 30 frames, update stats
	{
		fps_string    = "FPS:        ";
		stringstream ss1;
		ss1 << fps;
		fps_string.append( ss1.str() );

		unsigned int total	= 0;
		unsigned int CULLED = 0;
		for( unsigned int l = 0; l < NUM_LOD; l++ )
		{
			total += d_lod[l];
		}
		CULLED = NUM_AGENTS - total;

		culled_string = "CULLED:     ";
		stringstream ss2;
		ss2 << CULLED;
		culled_string.append( ss2.str() );

		lod1_string   = "LOD1:       ";
		stringstream ss3;

		ss3 << d_lod[0];
		lod1_string.append( ss3.str() );

		lod2_string   = "LOD2:       ";
		stringstream ss4;
		ss4 << d_lod[1];
		lod2_string.append( ss4.str() );

		lod3_string   = "LOD3:       ";
		stringstream ss5;
		ss5 << d_lod[2];
		lod3_string.append( ss5.str() );

		lod4_string   = "LOD4:       ";
		stringstream ss6;
		ss6 << d_lod[3];
		lod4_string.append( ss6.str() );

		delta_time_string = "DELTA TIME: ";
		stringstream ss7;
		ss7 << delta_time;
		delta_time_string.append( ss7.str() );
	}

	glutPostRedisplay();
	
}
//
//=======================================================================================
//
void drawPositionsTbo( void )
{
	fbo_manager->fbos["agents_fbo"].fbo->Bind();
	{
		fbo_manager->setTarget( "agents_pos_tex", true, true );
		fbo_manager->proj_manager->setOrthoProjection(	AGENTS_NPOT,
														AGENTS_NPOT	);
		glsl_manager->activate( "tbo" );
		{
			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_BUFFER, lca_path_manager->pos_tbo_id );
			glBindBuffer( GL_ARRAY_BUFFER, pos_tex_coords );
			{
				glEnableClientState			( GL_VERTEX_ARRAY								);
				glVertexPointer				( 4, GL_FLOAT, sizeof(Vertex), LOCATION_OFFSET	);
				glDrawArrays				( GL_POINTS, 0, pos_tex_size					);
				glDisableClientState		( GL_VERTEX_ARRAY								);
			}
			glBindBuffer( GL_ARRAY_BUFFER, 0 );
			glActiveTexture( GL_TEXTURE0 );
			glBindTexture( GL_TEXTURE_BUFFER, 0 );
		}
		glsl_manager->deactivate( "tbo" );
		fbo_manager->proj_manager->restoreProjection();
	}
	fbo_manager->fbos["agents_fbo"].fbo->Disable();
}
//
//=======================================================================================
//
void display( void )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	drawPositionsTbo();

//->RENDER_SCENE
	fbo_manager->fbos["display_fbo"].fbo->Bind();
	{
		camera->setView();
		float view_mat[16];
		glGetFloatv( GL_MODELVIEW_MATRIX, view_mat );

		float proj_mat[16];
		glGetFloatv( GL_PROJECTION_MATRIX, proj_mat );
		
		fbo_manager->setTarget( "display_tex", true, true );
		
		static_lod->runAssignmentCuda(	GL_TEXTURE_RECTANGLE,
										lca_path_manager->pos_tbo_id,
										fbo_manager->texture_ids["agents_ids_tex"],
										vbo_lod,
										camera								);
		static_lod->runSelectionCuda(	GL_TEXTURE_RECTANGLE,
										0,
										vbo_lod,
										camera								);

		for( unsigned int LOD = 0; LOD < NUM_LOD; LOD++ )
		{
			vbo_manager->render_instanced_culled_vbo(	nut[LOD]->getIds()[0],
														nut_tex,
														lca_path_manager->pos_tbo_id,
														vbo_lod[LOD].id,
														nut[LOD]->getSizes()[0],
														vbo_lod[LOD].primitivesWritten,
														view_mat						);

			d_lod[LOD] = vbo_lod[LOD].primitivesWritten;
		}

		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D, floor_tex );
		glPushMatrix();
		{
			glTranslatef( 0.0f, -25.0f, 0.0f );
			vbo_manager->render_vbo( floorm->getIds()[0], floorm->getSizes()[0] );
		}
		glPopMatrix();
		glBindTexture( GL_TEXTURE_2D, 0 );

		if( showstats )
		{
			axes->draw();
			glDisable( GL_LIGHTING );

			fbo_manager->proj_manager->setOrthoProjection(	0,
															0,
															VIEWPORT_HEIGHT/4,
															VIEWPORT_HEIGHT/4,
															0,
															AGENTS_NPOT,
															0,
															AGENTS_NPOT,
															true										);
			fbo_manager->pushActiveBind( "agents_pos_tex", 0 );
			{
				fbo_manager->renderQuad		(	"agents_fbo", 
												"pass_rect", 
												AGENTS_NPOT, 
												AGENTS_NPOT		);
			}
			fbo_manager->popActiveBind();
			fbo_manager->proj_manager->restoreProjection();
			
			//->DISPLAY_TEXT		
			displayText( 10,  20, (char*)fps_string.c_str()				);
			displayText( 10,  40, (char*)delta_time_string.c_str()		);
			displayText( 10,  60, (char*)culled_string.c_str()			);
			displayText( 10, 100, (char*)lod1_string.c_str()			);
			displayText( 10, 120, (char*)lod2_string.c_str()			);
			displayText( 10, 140, (char*)lod3_string.c_str()			);
			displayText( 10, 160, (char*)lod4_string.c_str()			);
			//<-DISPLAY_TEXT

			glEnable( GL_LIGHTING );
		}
	}
	fbo_manager->fbos["display_fbo"].fbo->Disable();
//<-RENDER_SCENE
	
	fbo_manager->displayTexture( "pass_rect",
								 "display_tex",
								 fbo_manager->fbos["display_fbo"].fbo_width,
								 fbo_manager->fbos["display_fbo"].fbo_height );
	glFlush();
	glutSwapBuffers();
}
//
//=======================================================================================
//
void cleanup( void )
{
	FREE_INSTANCE( camera				);
	FREE_INSTANCE( glsl_manager			);
	
	FREE_INSTANCE( vbo_manager			);
	FREE_INSTANCE( fbo_manager			);

	TextureManager::freeInstance();
}
//
//=======================================================================================
//
void special( int key, int x, int y )
{
	//printf( "SKEY=%d\n", key );
	int mod = glutGetModifiers();
	switch( key ) 
	{
		case GLUT_KEY_LEFT:
			if( mod == GLUT_ACTIVE_CTRL )
			{
			}
			else
			{
				camera->moveLeft( 50 );
			}
			break;
		case GLUT_KEY_RIGHT:
			if( mod == GLUT_ACTIVE_CTRL )
			{
			}
			else
			{
				camera->moveRight( 50 );
			}
			break;
		case GLUT_KEY_UP:
			if( mod == GLUT_ACTIVE_CTRL )
			{
			}
			else
			{
				camera->moveForward( 50 );
			}
			break;
		case GLUT_KEY_DOWN:
			if( mod == GLUT_ACTIVE_CTRL )
			{
			}
			else
			{
				camera->moveBackward( 50 );
			}
			break;
	}
}
//
//=======================================================================================
//
void keyboard( unsigned char key, int x, int y )
{
	//printf( "KEY=%d\n", key );
	int pdi     = 0;
	vec3 camPos = vec3( 0.0f );
	vec3 camDir = vec3( 0.0f );
	vec3 camUp  = vec3( 0.0f );
	switch( key )
	{
	case 43:	//'+'
		predef_cam_index = (predef_cam_index + 1) % predef_cam_pos.size();
		camPos = vec3(	predef_cam_pos[predef_cam_index].x,
                        predef_cam_pos[predef_cam_index].y,
                        predef_cam_pos[predef_cam_index].z );
		camDir = vec3(  predef_cam_dir[predef_cam_index].x,
                        predef_cam_dir[predef_cam_index].y,
                        predef_cam_dir[predef_cam_index].z );
		camUp  = vec3(  predef_cam_up[predef_cam_index].x,
                        predef_cam_up[predef_cam_index].y,
                        predef_cam_up[predef_cam_index].z );
        camera->setPosition( camPos );
        camera->setDirection( camDir );
        camera->setUpVec( camUp );
        camera->setView();
		break;
	case 45:	//'-'
		pdi = (int)predef_cam_index;
		if( (pdi-1) >= 0  )
		{
			predef_cam_index = predef_cam_index - 1;
		}
		else
		{
			predef_cam_index = predef_cam_pos.size() - 1;
		}
		camPos = vec3(	predef_cam_pos[predef_cam_index].x,
                        predef_cam_pos[predef_cam_index].y,
                        predef_cam_pos[predef_cam_index].z );
		camDir = vec3(  predef_cam_dir[predef_cam_index].x,
                        predef_cam_dir[predef_cam_index].y,
                        predef_cam_dir[predef_cam_index].z );
		camUp  = vec3(  predef_cam_up[predef_cam_index].x,
                        predef_cam_up[predef_cam_index].y,
                        predef_cam_up[predef_cam_index].z );
        camera->setPosition( camPos );
        camera->setDirection( camDir );
        camera->setUpVec( camUp );
        camera->setView();
		break;
	case 19:	//'CTRL+s'
		camera->moveDown( camAccel );
		camera->setView();
		break;
	case 23:	//'CTRL+w'
		camera->moveUp( camAccel );
		camera->setView();
		break;
	case 27:
		cleanup();
		exit( 0 );
		break;
	case 'a':
		camera->moveLeft( camAccel );
		camera->setView();
		break;
	case 'd':
		camera->moveRight( camAccel );
		camera->setView();
		break;
	case 'w':
		camera->moveForward( camAccel );
		camera->setView();
		break;
	case 's':
		camera->moveBackward( camAccel );
		camera->setView();
		break;
	case 'H':
		hideCharacters = !hideCharacters;
		break;
	case ' ':
		animating = !animating;
		break;
	case 'o':
		break;
	case 'e':
		break;
	case 'r':
		wireframe = !wireframe;
		break;
	case 'm':
		runpaths				= !runpaths;
		break;
	case '.':
		showstats = !showstats;
		glsl_manager->activate( "instancing_culled" );
		{
			if( showstats == false )
			{
				glsl_manager->setUniformf( "instancing_culled", "showLod", 0.0f );
			}
			else
			{
				glsl_manager->setUniformf( "instancing_culled", "showLod", 1.0f );
			}
		}
		glsl_manager->deactivate( "instancing_culled" );
		break;
	}
}
//
//=======================================================================================
//
void mouse( int button, int state, int x, int y )
{
	if( state == GLUT_DOWN ) 
	{
		if( button == GLUT_LEFT_BUTTON )
		{
			lMouseDown = true;
			rMouseDown = false;
			mMouseDown = false;
		}
		else if( button == GLUT_RIGHT_BUTTON )
		{
			lMouseDown = false;
			rMouseDown = true;
			mMouseDown = false;
		}
		else if( button ==  GLUT_MIDDLE_BUTTON  )
		{
			lMouseDown = false;
			rMouseDown = false;
			mMouseDown = true;
		}
	}
}
//
//=======================================================================================
//
void motion( int x, int y )
{
    vec3 up( 0.0f, 1.0f, 0.0f );
    vec3 ri( 1.0f, 0.0f, 0.0f );

	int modifiers = glutGetModifiers();
	if( modifiers & GLUT_ACTIVE_CTRL )
	{
		//printf( "GLUT_ACTIVE_CTRL\n" );
		ctrlDown = true;
	}
	else
	{
		ctrlDown = false;
	}

	static int xlast = -1, ylast = -1;
	int dx, dy;

	dx = x - xlast;
	dy = y - ylast;
	if( lMouseDown )
	{
		if( ctrlDown == false )
		{
			if( dx > 0 )
			{
				camera->rotateAngle(  camAccel/4.0f, up );
			}
			else if( dx < 0 )
			{
				camera->rotateAngle( -camAccel/4.0f, up );
			}
		}
		else
		{
			if( dy > 0 )
			{
				camera->rotateAngle( -camAccel/4.0f, ri );
			}
			else if( dy < 0 )
			{
				camera->rotateAngle(  camAccel/4.0f, ri );
			}
		}
	}
	else if( rMouseDown )
	{
		if( dy > 0 )
		{
			camera->rotateAngle( -camAccel/4.0f, ri );
		}
		else if( dy < 0 )
		{
			camera->rotateAngle(  camAccel/4.0f, ri );
		}
	}
	xlast = x;
	ylast = y;
}
//
//=======================================================================================
//
int main( int argc, char* argv[] )
{

	log_manager		= new LogManager( string("log.html") );
	err_manager		= new GlErrorManager( log_manager );

///////////////////////////////////////////////////////////////////////////
	camera = new Camera( 1, Camera::FREE, Frustum::RADAR );
	camera->getFrustum()->setNearD( 1.0f );
	camera->getFrustum()->setFarD( 50000.0f );
	camera->getFrustum()->setFovY( 54.43f );
	camera->setEyeSeparation( 35.0f );
#ifdef CAM_PERSP
	vec3 camPos( 0.0f, 500.0f, 2000.0f );
	vec3 camDir( 0.0f, 0.0f, -1.0f );
	vec3 camUp( 0.0f, 1.0f, 0.0f );
	camera->setPosition( camPos );
	camera->setDirection( camDir );
	camera->setUpVec( camUp );
#endif

	predef_cam_pos.push_back( glm::vec3(	camera->getPosition().x,
											camera->getPosition().y,
											camera->getPosition().z )	);
	predef_cam_dir.push_back( glm::vec3(	camera->getDirection().x,
											camera->getDirection().y,
											camera->getDirection().z )	);
	predef_cam_up.push_back( glm::vec3(	camera->getUp().x,
										camera->getUp().y,
										camera->getUp().z )	);
///////////////////////////////////////////////////////////////////////////

	// SEED_PSEUDO_RANDOM_VALUES_GENERATION:
	srand( (unsigned int) time( NULL ) );
	//srand( 1 );

	// INIT_GLUT:
	glutInit				(	&argc, 
								argv						);

	glutInitDisplayMode		(	GLUT_RGBA   | 
								GLUT_DEPTH  | 
								GLUT_DOUBLE					);
	glutInitWindowSize		(	INIT_WINDOW_WIDTH, 
								INIT_WINDOW_HEIGHT			);
	//glutInitWindowSize		(	1920, 
	//							1080						);
	//glutInitWindowSize		(	1280, 
	//							720							);
	glutInitWindowPosition	(	200,  
								50							);
	glutInitContextVersion	(	3, 
								1							);
	glutCreateWindow		(	title.c_str()				);
	glutInitContextFlags	(	GLUT_FORWARD_COMPATIBLE		);
	glutInitContextProfile	(	GLUT_COMPATIBILITY_PROFILE	);
	// Use this if you have context problems:
	glewExperimental = GL_TRUE;
	// INIT_GLEW:
	int glew_status = glewInit();
    if( glew_status != GLEW_OK )
    {
		printf( "GLEW initialization failed!.\n" );
		cleanup();
		exit( 0 );
    }

	// INIT_OPENGL:
	init();
	//glutFullScreen();

	printf( "\n\n------------------------------------------------------\n" );
	printf( "Vendor: %s\n",		glGetString (GL_VENDOR));
	printf( "Renderer: %s\n",	glGetString (GL_RENDERER));
	printf( "Version: %s\n",	glGetString (GL_VERSION));
	printf( "GLSL: %s\n",		glGetString (GL_SHADING_LANGUAGE_VERSION));
	printf( "------------------------------------------------------\n\n" );

	TextureManager::getInstance()->init( err_manager, log_manager );

//------------------------------------------------------------------------------------------
//->INIT_SHADER_MANAGER
	glsl_manager = new GlslManager( err_manager, log_manager );
	vector<InputShader*> input_shaders;
	
	InputShader* input_lambert = new InputShader( string( "lambert" ) );
	input_lambert->s_vert = string( "shaders/lambert.vert" );
	input_lambert->s_frag = string( "shaders/lambert.frag" );
	input_lambert->s_uni_i["diffuseTexture"] = 0;
	input_shaders.push_back( input_lambert );

	InputShader* input_gouraud = new InputShader( string( "gouraud" ) );
	input_gouraud->s_vert = string( "shaders/gouraud.vert" );
	input_gouraud->s_frag = string( "shaders/gouraud.frag" );
	input_gouraud->s_uni_i["diffuseTexture"] = 0;
	input_shaders.push_back( input_gouraud );

	InputShader* input_instancing = new InputShader( string( "instancing" ) );
	input_instancing->s_vert = string( "shaders/instancing.vert" );
	input_instancing->s_frag = string( "shaders/instancing.frag" );
	input_instancing->s_uni_i["diffuseTexture"] = 0;
	input_shaders.push_back( input_instancing );

	InputShader* input_tbo = new InputShader( string( "tbo" ) );
	input_tbo->s_vert = string( "shaders/drawTextureBufferVertex.glsl" );
	input_tbo->s_frag = string( "shaders/drawTextureBufferFragment.glsl" );
	input_tbo->s_uni_i["posTextureBuffer"]	= 0;
	input_tbo->s_uni_f["PLANE_SCALE"]		= PLANE_SCALE;
	input_shaders.push_back( input_tbo );

	InputShader* input_instancing_culled = new InputShader( string( "instancing_culled" ) );
	input_instancing_culled->s_vert = string( "shaders/instancing_culled.vert" );
	input_instancing_culled->s_frag = string( "shaders/instancing_culled.frag" );
	input_instancing_culled->s_uni_i["diffuseTexture"] = 0;
	input_instancing_culled->s_uni_i["idsTextureBuffer"] = 1;
	input_instancing_culled->s_uni_i["posTextureBuffer"] = 2;
	input_instancing_culled->s_uni_f["showLod"] = 0.0f;
	input_instancing_culled->s_uni_f["showShadow"] = 0.0f;
	input_shaders.push_back( input_instancing_culled );

	InputShader* input_vfc_lod_ass = new InputShader( string( "vfc_lod_assignment" ) );
	input_vfc_lod_ass->is_transform_feedback = true;
	input_vfc_lod_ass->s_vert = string( "shaders/VFCLodAssigmentTexVertex.glsl" );
	input_vfc_lod_ass->s_geom = string( "shaders/VFCLodAssigmentTexGeometryCuda.glsl" );
	input_vfc_lod_ass->transform_feedback_vars.push_back( string( "gl_Position" ) );
	input_vfc_lod_ass->s_uni_i["idTex"]		= 2;
	input_vfc_lod_ass->s_uni_i["positions"] = 7;
	input_vfc_lod_ass->s_ipri = GL_POINTS;
	input_vfc_lod_ass->s_opri = GL_POINTS;
	input_shaders.push_back( input_vfc_lod_ass );

	InputShader* input_vfc_lod_sel = new InputShader( string( "vfc_lod_selection" ) );
	input_vfc_lod_sel->is_transform_feedback = true;
	input_vfc_lod_sel->s_vert = string( "shaders/VFCLodAssigmentTexVertex.glsl" );
	input_vfc_lod_sel->s_geom = string( "shaders/lodSelectionGeometry.glsl" );
	input_vfc_lod_sel->transform_feedback_vars.push_back( string( "gl_Position" ) );
	input_vfc_lod_sel->s_ipri = GL_POINTS;
	input_vfc_lod_sel->s_opri = GL_POINTS;
	input_shaders.push_back( input_vfc_lod_sel );

	InputShader* input_pass_rect = new InputShader( string( "pass_rect" ) );
	input_pass_rect->s_vert = string( "shaders/passthru_rect_vertex.glsl" );
	input_pass_rect->s_frag = string( "shaders/passthru_rect_fragment.glsl" );
	input_pass_rect->s_uni_i["tex"] = 0;
	input_shaders.push_back( input_pass_rect );

	InputShader* input_pass_2d = new InputShader( string( "pass_2d" ) );
	input_pass_2d->s_vert = string( "shaders/passthru_2d_vertex.glsl" );
	input_pass_2d->s_frag = string( "shaders/passthru_2d_fragment.glsl" );
	input_pass_2d->s_uni_i["tex"] = 0;
	input_shaders.push_back( input_pass_2d );
	
	err_manager->getError( "Before init shaders:" );

	if( !glsl_manager->init( input_shaders ) )
	{
		printf( "Error initializing shaders.\n" );
		cleanup();
		exit( 1 );
	}
//<-INIT_SHADER_MANAGER
//------------------------------------------------------------------------------------------
//->INIT_VBO_MANAGER
	vbo_manager = new VboManager( log_manager, err_manager, glsl_manager );
	vbo_manager->setInstancingLocations				(	string( "instancing"				),
														string( "positions"					),
														string( "rotations"					),
														string( "scales"					),
														string( "normal"					),
														string( "texCoord0"					),
														string( "ViewMat4x4"				) );
	vbo_manager->setInstancingCulledLocations		(	string( "instancing_culled"			),
														string( "normal"					),
														string( "texCoord0"					),
														string( "ViewMat4x4"				) );
	err_manager->getError( "After setting instancing locations:" );

//<-INIT_VBO_MANAGER
//------------------------------------------------------------------------------------------
//->INIT_FBO_MANAGER
	vector<InputFbo*> fbo_inputs;

	vector<InputFboTexture*> input_fbo_textures1;
	InputFboTexture* tex0 = new InputFboTexture( string( "display_tex" ),       InputFbo::TYPICAL			);
	InputFboTexture* tex1 = new InputFboTexture( string( "shadow_tex"  ),       InputFbo::TYPICAL			);
	InputFboTexture* tex2 = new InputFboTexture( string( "display_depth_tex" ), InputFbo::DEPTH_NO_COMPARE	);
	input_fbo_textures1.push_back( tex0 );
	input_fbo_textures1.push_back( tex1 );
	input_fbo_textures1.push_back( tex2 );
	InputFbo* fbo1 = new InputFbo(	string( "display_fbo" ), 
									GL_TEXTURE_RECTANGLE, 
									input_fbo_textures1, 
									INIT_WINDOW_WIDTH, 
									INIT_WINDOW_HEIGHT );
	fbo_inputs.push_back( fbo1 );

	vector<InputFboTexture*> input_fbo_textures2;
	InputFboTexture* tex3 = new InputFboTexture( string( "shadow_display_tex" ),	InputFbo::TYPICAL			);
	InputFboTexture* tex4 = new InputFboTexture( string( "shadow_depth_tex" ),		InputFbo::DEPTH_NO_COMPARE	);
	input_fbo_textures2.push_back( tex3 );
	input_fbo_textures2.push_back( tex4 );
	InputFbo* fbo2 = new InputFbo(	string( "shadow_fbo" ), 
									GL_TEXTURE_RECTANGLE, 
									input_fbo_textures2, 
									VIEWPORT_WIDTH  * SHADOW_MAP_RATIO, 
									VIEWPORT_HEIGHT * SHADOW_MAP_RATIO );
	fbo_inputs.push_back( fbo2 );

	vector<InputFboTexture*> input_fbo_textures3;
	InputFboTexture* tex5 = new InputFboTexture( string( "depth_display_tex" ),	InputFbo::TYPICAL			);
	InputFboTexture* tex6 = new InputFboTexture( string( "depth_depth_tex" ),	InputFbo::DEPTH_NO_COMPARE	);
	input_fbo_textures3.push_back( tex5 );
	input_fbo_textures3.push_back( tex6 );
	InputFbo* fbo3 = new InputFbo(	string( "depth_fbo" ), 
									GL_TEXTURE_RECTANGLE, 
									input_fbo_textures3, 
									VIEWPORT_WIDTH  * SHADOW_MAP_RATIO, 
									VIEWPORT_HEIGHT * SHADOW_MAP_RATIO );
	fbo_inputs.push_back( fbo3 );

	vector<InputFboTexture*> agents_input_fbo_textures;
	InputFboTexture* texA = new InputFboTexture( string( "agents_pos_tex" ),   InputFbo::GPGPU	);
	InputFboTexture* texB = new InputFboTexture( string( "agents_ids_tex" ),   InputFbo::GPGPU	);
	string depth_name = "agents";
	depth_name.append( "_depth" );
	InputFboTexture* texC = new InputFboTexture( depth_name, InputFbo::DEPTH_NO_COMPARE	);
	agents_input_fbo_textures.push_back( texA );
	agents_input_fbo_textures.push_back( texB );
	agents_input_fbo_textures.push_back( texC );
	InputFbo* ai_fbo = new InputFbo(	string( "agents_fbo" ),
										GL_TEXTURE_RECTANGLE,
										agents_input_fbo_textures,
										AGENTS_NPOT,
										AGENTS_NPOT				);
	fbo_inputs.push_back( ai_fbo );

	fbo_manager = new FboManager( log_manager, glsl_manager, fbo_inputs );
	if( !fbo_manager->init() )
	{
		printf( "Error initializing FBOs!\n" );
		cleanup();
		exit( 1 );
	}

//->INIT_IDS
	vector<float> instance_ids_flat;
	for( unsigned int n = 0; n < NUM_AGENTS; n++ )
	{
		instance_ids_flat.push_back( (float)0		);	//GROUP_ID
		instance_ids_flat.push_back( (float)n		);	//UNIQUE_ID
		instance_ids_flat.push_back( (float)0.0f	);	//W
		instance_ids_flat.push_back( (float)0.0f	);	//LOD
	}
	float* data = new float[AGENTS_NPOT*AGENTS_NPOT*4];
	for( unsigned int i = 0; i < AGENTS_NPOT*AGENTS_NPOT*4; i++ )
	{
		data[i] = 0.0f;
	}
	for( unsigned int i = 0; i < instance_ids_flat.size(); i++ )
	{
		data[i] = instance_ids_flat[i];
	}
	glBindTexture( fbo_manager->fbos["agents_fbo"].fbo_tex_target, fbo_manager->texture_ids["agents_ids_tex"] );
	{
		glTexSubImage2D( fbo_manager->fbos["agents_fbo"].fbo_tex_target,
						 0,
						 0,
						 0,
						 fbo_manager->fbos["agents_fbo"].fbo_width,
						 fbo_manager->fbos["agents_fbo"].fbo_height,
						 GL_RGBA,
						 GL_FLOAT,
						 data										);
	}
	glBindTexture( fbo_manager->fbos["agents_fbo"].fbo_tex_target, 0 );
	delete data;
//<-INIT_IDS

//->INIT_POSITIONS
	lca_path_manager = new LCACudaPathManager( log_manager, vbo_manager );
	vector<float> pos;
	vector<float> goals;
	vector<glm::vec4> goal;

	float scene_width_in_cells	= (float)(3.0 * sqrtf( (float)NUM_AGENTS ) );
	float tile_width			= (PLANE_SCALE * 2.0f) / scene_width_in_cells;
	float radius				= tile_width / 2.0f;
	float r2					= (radius + radius) * (radius + radius);
	bool collision				= false;

	goal.push_back( glm::vec4( -PLANE_SCALE + tile_width / 2.0f, 0.0f, -PLANE_SCALE + tile_width / 2.0f, 1.0f ) );
	goal.push_back( glm::vec4(  PLANE_SCALE - tile_width / 2.0f, 0.0f, -PLANE_SCALE + tile_width / 2.0f, 1.0f ) );
	goal.push_back( glm::vec4(  PLANE_SCALE - tile_width / 2.0f, 0.0f,  PLANE_SCALE - tile_width / 2.0f, 1.0f ) );
	goal.push_back( glm::vec4( -PLANE_SCALE + tile_width / 2.0f, 0.0f,  PLANE_SCALE - tile_width / 2.0f, 1.0f ) );

	for( unsigned int i = 0; i < AGENTS_NPOT; i++ )
	{
		for( unsigned int j = 0; j < AGENTS_NPOT; j++ )
		{
			float x = 0.0f;
			float z = 0.0f;
			do
			{
				collision = false;
				x = PLANE_SCALE*((float)rand()/(float)RAND_MAX);
				if( rand()%100 > 50 )
				{
					x = -x;
				}
				z = PLANE_SCALE*((float)rand()/(float)RAND_MAX);
				if( rand()%100 > 50 )
				{
					z = -z;
				}
				for( unsigned int p = 0; p < pos.size(); p += 4 )
				{
					float dist = (pos[p]-x)*(pos[p]-x) + (pos[p+2]-z)*(pos[p+2]-z);
					if( dist < r2 )
					{
						collision = true;
						break;
					}
				}
			}
			while( collision );
			pos.push_back( x	);
			pos.push_back( 0.0f );
			pos.push_back( z	);
			pos.push_back( 1.0f );

			unsigned int index = j * AGENTS_NPOT + i;
			unsigned int g = index % 4;
			goals.push_back( goal[g].x );
			goals.push_back( goal[g].y );
			goals.push_back( goal[g].z );
			goals.push_back( goal[g].w );
		}
	}

	obstacle_manager = new ObstacleManager( log_manager, PLANE_SCALE * 2.0f, PLANE_SCALE * 2.0f );
	string csv = string( "assets/mdp/mdp_5x5.csv" );
	if( obstacle_manager->init( csv ) == false )
	{
		log_manager->log( LogManager::LERROR, "While initializing OBSTACLE_MANAGER!" );
		cleanup();
		exit( 1 );
	}

	if( lca_path_manager->init(	PLANE_SCALE * 2.0f,
								PLANE_SCALE * 2.0f,
								obstacle_manager->getMDPSceneWidthInTiles(),
								obstacle_manager->getMDPSceneDepthInTiles(),
								obstacle_manager->getMDPSceneWidthInTiles() * LCA_RATIO,
								obstacle_manager->getMDPSceneDepthInTiles() * LCA_RATIO,
								obstacle_manager->getMDPPolicy(),
								pos,
								goals ) == false										)
	{
		log_manager->log( LogManager::LERROR, "While initializing LCA_PATH_MANAGER!" );
		cleanup();
		exit( 1 );
	}

	initTexCoords();
//<-INIT_POSITIONS

//<-INIT_FBO_MANAGER
//------------------------------------------------------------------------------------------

	axes = new Axes( 100.0f );

	string mrpath( "assets" );
	string tfname( "assets/nut.tga" );

	string mname( "nut" );
	string mname0( "nut0" );
	string mfname0( "nut_lod0.obj" );
	string mname1( "nut1" );
	string mfname1( "nut_lod1.obj" );
	string mname2( "nut2" );
	string mfname2( "nut_lod2.obj" );
	string mname3( "nut3" );
	string mfname3( "nut_lod3.obj" );
	
	vector<string> mnames;
	vector<string> mfnames;

	mnames.push_back( mname0 );
	mnames.push_back( mname1 );
	mnames.push_back( mname2 );
	mnames.push_back( mname3 );

	mfnames.push_back( mfname0 );
	mfnames.push_back( mfname1 );
	mfnames.push_back( mfname2 );
	mfnames.push_back( mfname3 );


	string fmname( "floor" );
	string ffname( "floor.obj" );
	string ftname( "assets/nut.tga" );
	floorm = new Model3D( fmname, mrpath, vbo_manager, glsl_manager, ffname, PLANE_SCALE/5.0f );
	floorm->init( true );
	floor_tex = TextureManager::getInstance()->loadTexture( ftname, false, GL_TEXTURE_2D, GL_MODULATE );

	for( unsigned int l = 0; l < NUM_LOD; l++ )
	{
		Model3D* model = new Model3D( mnames[l], mrpath, vbo_manager, glsl_manager, mfnames[l], 50.0f );
		model->init( true );
		nut.push_back( model );
		d_lod.push_back( 0 );
	}
	nut_tex = TextureManager::getInstance()->loadTexture( tfname, false, GL_TEXTURE_2D, GL_MODULATE );

	static_lod = new StaticLod( glsl_manager, vbo_manager, err_manager, mname );
	static_lod->init( AGENTS_NPOT, AGENTS_NPOT );
	for( unsigned int l = 0; l < NUM_LOD; l++ )
	{
		vbo_lod[l].id					= 0;
		vbo_lod[l].primitivesGenerated	= 0;
		vbo_lod[l].primitivesWritten	= 0;
	}

	// REGISTER_GLUT_CALLBACKS:
	glutDisplayFunc		( display	);
	glutIdleFunc		( idle		);
	glutReshapeFunc		( reshape	);
	glutKeyboardFunc	( keyboard	);
	glutMouseFunc		( mouse		);
	glutMotionFunc		( motion	);
	glutSpecialFunc		( special	);


	// REPORT TEXTURES:
	printf( "\n\n" );
	printf( "---------------------\n" );
	float totalWeight = 0.0f;
	unsigned int totalTextures = 0;
	printf( "TEXTURES 2D:\n" );
	printf( "---------------------\n" );
	textureList textures = TextureManager::getInstance()->getTextures();
	for( tListItor itor = textures.begin(); itor != textures.end(); ++itor )
	{
		string filename		= (*itor)->getName();
		vector<string> sep	= StringUtils::split( filename, '/' );
		string base_name	= sep[sep.size() - 1];
		float wKb			= ((float)((*itor)->getWeightB())/1024.0f)/1024.0f;
		printf( "%08.4f MB\t%dx%d\t\"%s\"\n", wKb, (*itor)->getWidth(), (*itor)->getHeight(), base_name.c_str() );
		totalWeight += wKb;
		totalTextures++;
	}
	printf( "---------------------\n" );
	printf( "TEXTURES 3D:\n" );
	printf( "---------------------\n" );
	texture3DList textures3D = TextureManager::getInstance()->getTextures3D();
	for( t3DListItor itor3D = textures3D.begin(); itor3D != textures3D.end(); ++itor3D )
	{
		string filename		= (*itor3D)->getName();
		vector<string> sep	= StringUtils::split( filename, '/' );
		string base_name	= sep[sep.size() - 1];
		float wKb			= ((float)((*itor3D)->getWeightB())/1024.0f)/1024.0f;
		printf( "%08.4f MB\t%dx%dx%d\t\"%s\"\n", wKb, (*itor3D)->getWidth(), (*itor3D)->getHeight(), (*itor3D)->getDepth(), base_name.c_str() );
		totalWeight += wKb;
		totalTextures++;
	}
	printf( "---------------------\n" );
	printf( "TOTAL: %08.4f MB in %d textures.\n", totalWeight, totalTextures );
	printf( "---------------------\n" );
	printf( "\n" );


	err_manager->getError( "Before rendering:" );

	// BEGIN_RENDER:
	glutMainLoop();

	// EXIT_PROGRAM:
	return 0;
}

void initTexCoords( void )
{
	unsigned int pos_tc_index = 0;
	unsigned int pos_tc_frame = 0;
	string vbo_name = "POS_TEX_COORDS_CUDA";
	pos_tc_index = vbo_manager->createVBOContainer( vbo_name, pos_tc_frame );
	
	unsigned int i, j;
	for( i = 0; i < AGENTS_NPOT; i++ )
	{
		for( j = 0; j < AGENTS_NPOT; j++ )
		{
			Vertex v;
			INITVERTEX( v );
			v.location[0] = (float)i;
			v.location[1] = (float)j;
			v.location[2] = 0.0f;
			v.location[3] = 1.0f;
			vbo_manager->vbos[pos_tc_index][pos_tc_frame].vertices.push_back( v );
		}
	}
	
	vbo_manager->gen_vbo( pos_tex_coords, pos_tc_index, pos_tc_frame );
	pos_tex_size = vbo_manager->vbos[pos_tc_index][pos_tc_frame].vertices.size();
}
