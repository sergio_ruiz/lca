// @REV_TAG SRL_03_2014
//
//													 Sergio Ruiz Loza. A01106919@itesm.mx
//																	  All Rights Reserved
//													  ITESM-CCM (http://www.ccm.itesm.mx)
//												 Computer Science Ph.D. Thesis Subproject
//										   Global Thesis Project: "Crowd Motion Planning"
//							Thesis Director: Dr. Benjamín Hernández A. hbenjamin@itesm.mx
//											   Programmed in C++ for OpenGL 3.1 and newer
//  														   Requires GLEW and FREEGLUT
//
//
//															   See ReadMe.txt for details
//
//=======================================================================================

#include "cLCACudaPathManager.h"

//=======================================================================================
//
//lca_paths_kernel.cu
extern "C" void launch_lca_kernel		(	std::vector<float>&	pos,
											std::vector<float>&	goal,
											std::vector<float>& lca,
											float				scene_width,
											float				scene_depth,
											unsigned int		mesh_width,
											unsigned int		mesh_depth,
											float				time			);
//
//=======================================================================================
//
extern "C" void init_cells_and_agents	(	std::vector<float>&	pos,
											std::vector<float>&	goal,
											std::vector<float>&	lca,
											float				scene_width,
											float				scene_depth,
											unsigned int		lca_width,
											unsigned int		lca_depth,
											unsigned int		num_agents,
											bool&				result			);
//
//=======================================================================================
//
extern "C" void cleanup_lca				(	void								);
//
//=======================================================================================
//
LCACudaPathManager::LCACudaPathManager( LogManager*		log_manager,
										VboManager*		vbo_manager	)
{
	this->log_manager		= log_manager;
	this->vbo_manager		= vbo_manager;

	string vbo_name;
	vbo_name				= string( "cuda_positions" );
	cuda_pos_vbo_frame		= 0;
	cuda_pos_vbo_index		=
	//	vbo_manager->createGlCudaVboContainer( vbo_name,
	//										   cuda_pos_vbo_frame					);
	//log_manager->log( LogManager::CUDA, "Created GL-CUDA positions container."		);
		vbo_manager->createVBOContainer( vbo_name,
											   cuda_pos_vbo_frame					);
	log_manager->log( LogManager::CUDA, "Created GL positions container."			);
	cuda_pos_vbo_id			= 0;
	cuda_pos_vbo_size		= 0;

	vbo_name				= string( "cuda_goals" );
	cuda_goals_vbo_frame	= 0;
	cuda_goals_vbo_index	=
	//	vbo_manager->createGlCudaVboContainer( vbo_name,
	//										   cuda_goals_vbo_frame					);
	//log_manager->log( LogManager::CUDA, "Created GL-CUDA goals container."			);
		vbo_manager->createVBOContainer( vbo_name,
											   cuda_goals_vbo_frame					);
	log_manager->log( LogManager::CUDA, "Created GL goals container."				);
	cuda_goals_vbo_id		= 0;
	cuda_goals_vbo_size		= 0;

	pos_tbo_id				= 0;

	cudaSetDevice( getMaxGflopsDeviceId() );
	cudaGLSetGLDevice( getMaxGflopsDeviceId() );

	scene_width				= 0.0f;
	scene_depth				= 0.0f;

	mdp_width				= 0;
	mdp_depth				= 0;

	lca_width				= 0;
	lca_depth				= 0;

	lca_cells_in_mdp_cell	= 0;
	lca_in_mdp_width		= 0;
	lca_in_mdp_depth		= 0;
}
//
//=======================================================================================
//
LCACudaPathManager::~LCACudaPathManager( void )
{
	positions.clear();
	goals.clear();
}
//
//=======================================================================================
//
bool LCACudaPathManager::init(	float			_scene_width,
								float			_scene_depth,
								unsigned int	_mdp_width,
								unsigned int	_mdp_depth,
								unsigned int	_lca_width,
								unsigned int	_lca_depth,
								vector<float>&	_mdp_policy,
								vector<float>&	_init_pos,
								vector<float>&	_goals			)
{
	scene_width		= _scene_width;
	scene_depth		= _scene_depth;
	mdp_width		= _mdp_width;
	mdp_depth		= _mdp_depth;
	lca_width		= _lca_width;
	lca_depth		= _lca_depth;
	mdp_policy		= _mdp_policy;
	positions		= _init_pos;
	goals			= _goals;
	if( setLCAExits() )
	{
		return init();
	}
	else
	{
		log_manager->log( LogManager::LERROR, "While setting LCA Exits." );
		return false;
	}
}
//
//=======================================================================================
//
bool LCACudaPathManager::init( void )
{
	if( positions.size() % 4 == 0 )
	{
		for( unsigned int i = 0; i < positions.size(); i += 4 )
		{
			Vertex v;
			INITVERTEX( v );
			v.location[0]		= positions[i+0];
			v.location[1]		= positions[i+1];
			v.location[2]		= positions[i+2];
			v.location[3]		= positions[i+3];
			//vbo_manager->gl_cuda_vbos[cuda_pos_vbo_index][cuda_pos_vbo_frame].vertices.push_back( v );
			vbo_manager->vbos[cuda_pos_vbo_index][cuda_pos_vbo_frame].vertices.push_back( v );
		}

		cuda_pos_vbo_size		= vbo_manager->gen_vbo(		cuda_pos_vbo_id,
															cuda_pos_vbo_index,
															cuda_pos_vbo_frame	);
		/*
		cuda_pos_vbo_size  = vbo_manager->gen_gl_cuda_vbo2(	cuda_pos_vbo_id,
															cuda_pos_vbo_res,
															cuda_pos_vbo_index,
															cuda_pos_vbo_frame	);
		*/

		log_manager->log( LogManager::CUDA, "Generated GL-CUDA positions VBO. Vertices: %u (%uKB).",
										    cuda_pos_vbo_size,
										    cuda_pos_vbo_size * sizeof( float ) / 1024					);

		// ALSO_ATTACH_A_TEXTURE_BUFFER_OBJECT:
		glGenTextures	( 1, &pos_tbo_id									);
		glActiveTexture ( GL_TEXTURE7										);
		glBindTexture	( GL_TEXTURE_BUFFER, pos_tbo_id						);
		glTexBuffer		( GL_TEXTURE_BUFFER, GL_RGBA32F, cuda_pos_vbo_id	);
		glBindTexture	( GL_TEXTURE_BUFFER, (GLuint)0						);


		for( unsigned int i = 0; i < goals.size(); i += 4 )
		{
			Vertex v;
			INITVERTEX( v );
			v.location[0]		= goals[i+0];
			v.location[1]		= goals[i+1];
			v.location[2]		= goals[i+2];
			v.location[3]		= goals[i+3];
			//vbo_manager->gl_cuda_vbos[cuda_goals_vbo_index][cuda_goals_vbo_frame].vertices.push_back( v );
			vbo_manager->vbos[cuda_goals_vbo_index][cuda_goals_vbo_frame].vertices.push_back( v );
		}
		cuda_goals_vbo_size		= vbo_manager->gen_vbo(		cuda_goals_vbo_id,
															cuda_goals_vbo_index,
															cuda_goals_vbo_frame	);
		/*
		cuda_goals_vbo_size  = vbo_manager->gen_gl_cuda_vbo2(	cuda_goals_vbo_id,
																cuda_goals_vbo_res,
																cuda_goals_vbo_index,
																cuda_goals_vbo_frame	);
		*/
		log_manager->log( LogManager::CUDA, "Generated GL-CUDA goals VBO. Vertices: %u (%uKB).",
										    cuda_goals_vbo_size,
										    cuda_goals_vbo_size * sizeof( float ) / 1024				);

		glBindBuffer					(	GL_ARRAY_BUFFER, 
											cuda_pos_vbo_id						);
		glBufferSubData					(	GL_ARRAY_BUFFER,
											0,
											sizeof(float) * positions.size(), 
											&positions[0]						);
		glBindBuffer					(	GL_ARRAY_BUFFER,
											0									);

		bool result = false;
		init_cells_and_agents			(	positions,
											goals,
											lca_exits,
											scene_width,
											scene_depth,
											lca_width,
											lca_depth,
											positions.size() / 4,
											result								);

		if( result )
		{
			log_manager->log( LogManager::CUDA, "LCA Kernel initialized successfully." );
		}
		else
		{
			log_manager->log( LogManager::LERROR, "While initializing LCA Kernel." );
		}
		return result;
	}
	else
	{
		log_manager->log( LogManager::LERROR, "Wrong CUDA paths inputs size." );
		return false;
	}
}
//
//=======================================================================================
//
bool LCACudaPathManager::setLCAExits( void )
{
	if( fmod( (float)lca_width, (float)mdp_width ) > 0.0f )
	{
		log_manager->log( LogManager::LERROR, "MDP and LCA dimensions not compatible." );
		return false;
	}

	lca_in_mdp_width		= lca_width / mdp_width;
	lca_in_mdp_depth		= lca_depth / mdp_depth;
	lca_cells_in_mdp_cell	= lca_in_mdp_width * lca_in_mdp_depth;

	lca_exits.clear();
	for( unsigned int i = 0; i < lca_width * lca_depth; i++ )
	{
		lca_exits.push_back( 10.0f );
	}

	for( unsigned int lx = 0; lx < lca_width; lx++ )
	{
		for( unsigned int lz = 0; lz < lca_depth; lz++ )
		{
			unsigned int mx = lx / lca_in_mdp_width;
			unsigned int mz = lz / lca_in_mdp_depth;
			unsigned int mi = mz * mdp_width + mx;
			if( mdp_policy[ mi ] > 7.0f )
			{
				unsigned int li = lz * lca_width + lx;
				lca_exits[ li ] = mdp_policy[ mi ];
			}
		}
	}

	for( unsigned int mdp_x = 0; mdp_x < mdp_width; mdp_x++ )
	{
		for( unsigned int mdp_z = 0; mdp_z < mdp_depth; mdp_z++ )
		{
			unsigned int mdp_i		= mdp_z * mdp_width + mdp_x;
			// POLICY: 0=UP_LEFT 1=LEFT 2=DOWN_LEFT 3=DOWN 4=DOWN_RIGHT 5=RIGHT 6=UP_RIGHT 7=UP 8=WALL 9=EXIT
			float f_policy			= mdp_policy[ mdp_i ];
			unsigned int ui_policy	= (unsigned int)f_policy;
			vector<float> mdp_neighbor_walls;
			for( unsigned int i = 0; i < 8; i++ )
			{
				mdp_neighbor_walls.push_back( 0.0f );
			}
			if( mdp_x > 0 )
			{
				if( mdp_z > 0 )
				{
					unsigned int mdp_ul = (mdp_z - 1) * mdp_width + (mdp_x - 1);
					if( mdp_policy[ mdp_ul ] == 8.0f )
					{
						mdp_neighbor_walls[0] = 1.0f;
					}
				}

				unsigned int mdp_l = mdp_z * mdp_width + (mdp_x - 1);
				if( mdp_policy[ mdp_l ] == 8.0f )
				{
					mdp_neighbor_walls[1] = 1.0f;
				}

				if( mdp_z + 1 < mdp_depth )
				{
					unsigned int mdp_dl = (mdp_z + 1) * mdp_width + (mdp_x - 1);
					if( mdp_policy[ mdp_dl ] == 8.0f )
					{
						mdp_neighbor_walls[2] = 1.0f;
					}
				}
			}

			if( mdp_z + 1 < mdp_depth )
			{
				unsigned int mdp_d = (mdp_z + 1) * mdp_width + mdp_x;
				if( mdp_policy[ mdp_d ] == 8.0f )
				{
					mdp_neighbor_walls[3] = 1.0f;
				}
			}

			if( mdp_x + 1 < mdp_width )
			{
				if( mdp_z + 1 < mdp_depth )
				{
					unsigned int mdp_dr = (mdp_z + 1) * mdp_width + (mdp_x + 1);
					if( mdp_policy[ mdp_dr ] == 8.0f )
					{
						mdp_neighbor_walls[4] = 1.0f;
					}
				}

				unsigned int mdp_r = mdp_z * mdp_width + (mdp_x + 1);
				if( mdp_policy[ mdp_r ] == 8.0f )
				{
					mdp_neighbor_walls[5] = 1.0f;
				}

				if( mdp_z > 0 )
				{
					unsigned int mdp_ur = (mdp_z - 1) * mdp_width + (mdp_x + 1);
					if( mdp_policy[ mdp_ur ] == 8.0f )
					{
						mdp_neighbor_walls[6] = 1.0f;
					}
				}
			}

			if( mdp_z > 0 )
			{
				unsigned int mdp_u = (mdp_z - 1) * mdp_width + mdp_x;
				if( mdp_policy[ mdp_u ] == 8.0f )
				{
					mdp_neighbor_walls[7] = 1.0f;
				}
			}

			unsigned int lca_x		= mdp_x * lca_in_mdp_width;
			unsigned int lca_z		= mdp_z * lca_in_mdp_depth;
			unsigned int lca_i		= 0;

			switch( ui_policy )
			{
			case 0:	//UP_LEFT
				if( mdp_neighbor_walls[7] == 0.0f )
				{
					for( unsigned int x = lca_x + 1; x < lca_x + lca_in_mdp_width; x++ )	//UP
					{
						lca_i				= lca_z * lca_width + x;
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[1] == 0.0f )
				{
					for( unsigned int z = lca_z + 1; z < lca_z + lca_in_mdp_depth; z++ )	//LEFT
					{
						lca_i				= z * lca_width + lca_x;
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[7] == 0.0f && mdp_neighbor_walls[1] == 0.0f )
				{
					lca_i				= lca_z * lca_width + lca_x;
					lca_exits[lca_i]	= f_policy;
				}
				break;
			case 1:	//LEFT
				for( unsigned int z = lca_z; z < lca_z + lca_in_mdp_depth; z++ )	//LEFT
				{
					lca_i				= z * lca_width + lca_x;
					lca_exits[lca_i]	= f_policy;
				}
				break;
			case 2:	//DOWN_LEFT
				if( mdp_neighbor_walls[3] == 0.0f )
				{
					for( unsigned int x = lca_x + 1; x < lca_x + lca_in_mdp_width; x++ )	//DOWN
					{
						lca_i				= (lca_z + lca_in_mdp_depth - 1) * lca_width + x;
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[1] == 0.0f )
				{
					for( unsigned int z = lca_z; z < lca_z + lca_in_mdp_depth - 1; z++ )	//LEFT
					{
						lca_i				= z * lca_width + lca_x;
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[3] == 0.0f && mdp_neighbor_walls[1] == 0.0f )
				{
					lca_i				= (lca_z + lca_in_mdp_depth - 1) * lca_width + lca_x;
					lca_exits[lca_i]	= f_policy;
				}
				break;
			case 3:	//DOWN
				for( unsigned int x = lca_x; x < lca_x + lca_in_mdp_width; x++ )	//DOWN
				{
					lca_i				= (lca_z + lca_in_mdp_depth - 1) * lca_width + x;
					lca_exits[lca_i]	= f_policy;
				}
				break;
			case 4:	//DOWN_RIGHT
				if( mdp_neighbor_walls[3] == 0.0f )
				{
					for( unsigned int x = lca_x; x < lca_x + lca_in_mdp_width - 1; x++ )	//DOWN
					{
						lca_i				= (lca_z + lca_in_mdp_depth - 1) * lca_width + x;
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[5] == 0.0f )
				{
					for( unsigned int z = lca_z; z < lca_z + lca_in_mdp_depth - 1; z++ )	//RIGHT
					{
						lca_i				= z * lca_width + (lca_x + lca_in_mdp_width - 1);
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[3] == 0.0f && mdp_neighbor_walls[5] == 0.0f )
				{
					lca_i				= (lca_z + lca_in_mdp_depth - 1) * lca_width + (lca_x + lca_in_mdp_width - 1);
					lca_exits[lca_i]	= f_policy;
				}
				break;
			case 5:	//RIGHT
				for( unsigned int z = lca_z; z < lca_z + lca_in_mdp_depth; z++ )	//RIGHT
				{
					lca_i				= z * lca_width + (lca_x + lca_in_mdp_width - 1);
					lca_exits[lca_i]	= f_policy;
				}
				break;
			case 6:	//UP_RIGHT
				if( mdp_neighbor_walls[0] == 0.0f )
				{
					for( unsigned int x = lca_x; x < lca_x + lca_in_mdp_width - 1; x++ )	//UP
					{
						lca_i				= lca_z * lca_width + x;
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[5] == 0.0f )
				{
					for( unsigned int z = lca_z + 1; z < lca_z + lca_in_mdp_depth; z++ )	//RIGHT
					{
						lca_i				= z * lca_width + (lca_x + lca_in_mdp_width - 1);
						lca_exits[lca_i]	= f_policy;
					}
				}
				if( mdp_neighbor_walls[0] == 0.0f && mdp_neighbor_walls[5] == 0.0f )
				{
					lca_i				= lca_z * lca_width + (lca_x + lca_in_mdp_width - 1);
					lca_exits[lca_i]	= f_policy;
				}
				break;
			case 7:	//UP
				for( unsigned int x = lca_x; x < lca_x + lca_in_mdp_width; x++ )	//UP
				{
					lca_i				= lca_z * lca_width + x;
					lca_exits[lca_i]	= f_policy;
				}
				break;
			}
		}
	}

	printf( "\n\nLCA_TOPOLOGY:\n" );
	for( unsigned int z = 0; z < lca_depth; z++ )
	{
		for( unsigned int x = 0; x < lca_width; x++ )
		{
			unsigned int lca_i = z * lca_width + x;
			if( lca_exits[ lca_i ] == 8.0f )
			{
				printf( "%c ", 219 );
			}
			else if( lca_exits[ lca_i ] == 9.0f )
			{
				printf( "# " );
			}
			else if( lca_exits[ lca_i ] == 10.0f )
			{
				printf( "%c ", 176 );
			}
			else
			{
				switch ( (unsigned int)lca_exits[lca_i] )
				{
					case 0:		// UL
						printf( "%c ", 218 );
						break;
					case 1:		// LL
						printf( "%c ", 60 );
						break;
					case 2:		// DL
						printf( "%c ", 192 );
						break;
					case 3:		// DD
						printf( "%c ", 118 );
						break;
					case 4:		// DR
						printf( "%c ", 217 );
						break;
					case 5:		// RR
						printf( "%c ", 62 );
						break;
					case 6:		// UR
						printf( "%c ", 191 );
						break;
					case 7:		// UU
						printf( "%c ", 94 );
						break;
					default:	// ??
						printf( "%i ", (unsigned int)lca_exits[lca_i] );
						break;
				}
			}
		}
		printf( "\n" );
	}
	printf( "\n" );

	return true;
}
//
//=======================================================================================
//
void LCACudaPathManager::runCuda(	unsigned int	texture_width,
									unsigned int	texture_height,
									float			parameter		)
{
	launch_lca_kernel(	positions,
						goals,
						lca_exits,
						scene_width,
						scene_depth,
						texture_width,
						texture_height,
						parameter		);

	glBindBuffer					(	GL_ARRAY_BUFFER, 
										cuda_pos_vbo_id						);
	glBufferSubData					(	GL_ARRAY_BUFFER,
										0,
										sizeof(float) * positions.size(), 
										&positions[0]						);
	glBindBuffer					(	GL_ARRAY_BUFFER,
										0									);
}
//
//=======================================================================================
//
inline int LCACudaPathManager::getMaxGflopsDeviceId( void )
{
	// This function returns the best GPU (with maximum GFLOPS)
	int current_device   = 0;
	int sm_per_multiproc = 0;
	int max_compute_perf = 0;
	int max_perf_device  = 0;
	int device_count     = 0;
	int best_SM_arch     = 0;
	cudaDeviceProp deviceProp;

	cudaGetDeviceCount( &device_count );
	// Find the best major SM Architecture GPU device
	while( current_device < device_count )
	{
		cudaGetDeviceProperties( &deviceProp, current_device );
		if( deviceProp.major > 0 && deviceProp.major < 9999 )
		{
			best_SM_arch = std::max( best_SM_arch, deviceProp.major );
		}
		current_device++;
	}

    // Find the best CUDA capable GPU device
	current_device = 0;
	while( current_device < device_count )
	{
		cudaGetDeviceProperties( &deviceProp, current_device );
		if( deviceProp.major == 9999 && deviceProp.minor == 9999 )
		{
		    sm_per_multiproc = 1;
		}
		else
		{
			sm_per_multiproc = _ConvertSMVer2Cores( deviceProp.major, deviceProp.minor );
		}

		int compute_perf  =
			deviceProp.multiProcessorCount * sm_per_multiproc * deviceProp.clockRate;
		if( compute_perf  > max_compute_perf )
		{
            // If we find GPU with SM major > 2, search only these
			if( best_SM_arch > 2 )
			{
				// If our device==dest_SM_arch, choose this, or else pass
				if( deviceProp.major == best_SM_arch )
				{
					max_compute_perf  = compute_perf;
					max_perf_device   = current_device;
				}
			}
			else
			{
				max_compute_perf  = compute_perf;
				max_perf_device   = current_device;
			}
		}
		++current_device;
	}
	return max_perf_device;
}
//
//=======================================================================================
//
inline int LCACudaPathManager::_ConvertSMVer2Cores( int major, int minor )
{
	// Defines for GPU Architecture types
	// (using the SM version to determine the # of cores per SM)
	typedef struct {
		int SM; // 0xMm (hexidecimal notation),
				// M = SM Major version, and m = SM minor version
		int Cores;
	} sSMtoCores;

	sSMtoCores nGpuArchCoresPerSM[] =
	{ 
        { 0x10,  8 }, // Tesla Generation (SM 1.0) G80 class
        { 0x11,  8 }, // Tesla Generation (SM 1.1) G8x class
        { 0x12,  8 }, // Tesla Generation (SM 1.2) G9x class
        { 0x13,  8 }, // Tesla Generation (SM 1.3) GT200 class
        { 0x20, 32 }, // Fermi Generation (SM 2.0) GF100 class
        { 0x21, 48 }, // Fermi Generation (SM 2.1) GF10x class
        { 0x30, 192}, // Kepler Generation (SM 3.0) GK10x class
        { 0x35, 192}, // Kepler Generation (SM 3.5) GK11x class
        {   -1, -1 }
	};

	int index = 0;
	while( nGpuArchCoresPerSM[index].SM != -1 )
	{
		if( nGpuArchCoresPerSM[index].SM == ((major << 4) + minor) )
		{
			return nGpuArchCoresPerSM[index].Cores;
		}
		index++;
	}
	log_manager->log( LogManager::CUDA,
					  "MapSMtoCores undefined SMversion: %d.%d!",
					  major,
					  minor										);
	return -1;
}
//
//=======================================================================================
