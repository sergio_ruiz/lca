#include "cObstacleManager.h"

ObstacleManager::ObstacleManager(	LogManager*		_log_manager,
									float			_scene_width,
									float			_scene_depth )
{
	log_manager					= _log_manager;
	mdp_manager					= new MDPSquareManager();
	NQ							= 8;
	mdp_scene_width_in_tiles	= 0;
	mdp_scene_depth_in_tiles	= 0;
	scene_width					= _scene_width;
	scene_depth					= _scene_depth;
	mdp_tile_width				= 0.0f;
	mdp_tile_depth				= 0.0f;
}

ObstacleManager::~ObstacleManager( void )
{

}

bool ObstacleManager::init( string&	_mdp_csv_file )
{
	mdp_csv_file = _mdp_csv_file;
	return initMDP();
}

bool ObstacleManager::initMDP( void )
{
	if( mdp_manager->solve_from_csv( mdp_csv_file, mdp_policy ) )
	{
		mdp_manager->getRewards( mdp_topology );
		mdp_scene_depth_in_tiles	= mdp_manager->getRows();
		mdp_scene_width_in_tiles	= mdp_manager->getColumns();
		mdp_tile_width				= scene_width / (float)mdp_scene_width_in_tiles;
		mdp_tile_depth				= scene_depth / (float)mdp_scene_depth_in_tiles;
		log_manager->file_log(	LogManager::OBSTACLE_MANAGER,
								"MDP_INITED_OK::ROWS=%d::COLS=%d::ITERATIONS=%i",
								mdp_scene_depth_in_tiles,
								mdp_scene_width_in_tiles,
								mdp_manager->getIterations()					);
		return true;
	}
	else
	{
		log_manager->file_log( LogManager::LERROR, "ERROR_SOLVING_FROM_CSV::\"%s\"", mdp_csv_file.c_str() );
		return false;
	}
}

vector<float>& ObstacleManager::getMDPPolicy( void )
{
	return mdp_policy;
}

unsigned int ObstacleManager::getMDPSceneWidthInTiles( void )
{
	return mdp_scene_width_in_tiles;
}

unsigned int ObstacleManager::getMDPSceneDepthInTiles( void )
{
	return mdp_scene_depth_in_tiles;
}
