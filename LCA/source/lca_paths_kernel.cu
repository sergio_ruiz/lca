// @REV_TAG SRL_03_2014
//
//													 Sergio Ruiz Loza. A01106919@itesm.mx
//																	  All Rights Reserved
//													  ITESM-CCM (http://www.ccm.itesm.mx)
//												 Computer Science Ph.D. Thesis Subproject
//										   Global Thesis Project: "Crowd Motion Planning"
//							Thesis Director: Dr. Benjam�n Hern�ndez A. hbenjamin@itesm.mx
//											   Programmed in C++ for OpenGL 3.1 and newer
//  														   Requires GLEW and FREEGLUT
//
//
//															   See ReadMe.txt for details
//
//=======================================================================================

#ifndef __LCA_PATHS_KERNEL_H_
#define __LCA_PATHS_KERNEL_H_

#define __DEBUG
#define __PRINT_AGENTS
#define __PRINT_FUTURE_OCC

#pragma once
#include "ccLCA.h"
//
//=======================================================================================
//
float			HALF_SCENE_WIDTH_IN_CELLS;
float			TILE_WIDTH;
unsigned int	NUM_AGENTS;
unsigned int	SCENE_WIDTH_IN_CELLS;
unsigned int	TOTAL_CELLS;
unsigned int	COUNTER;
unsigned int	COLLISIONS;

float			agents_time;
float			mem_alloc_time;
float			total_time;
float			fix_coll_time;
float			virtual_advance_time;
float			advance_time;
float			temp_time;

cudaEvent_t		start;
cudaEvent_t		stop;

thrust::device_vector<unsigned int> cells_ids;
thrust::device_vector<unsigned int> d_cells_ids;
thrust::device_vector<unsigned int> cells_row;
thrust::device_vector<unsigned int> cells_col;
thrust::device_vector<unsigned int> cells_occ;
thrust::device_vector<unsigned int>	agents_ids;
thrust::device_vector<unsigned int>	agents_dids;
thrust::device_vector<unsigned int>	agents_goal_cell;
thrust::device_vector<unsigned int>	agents_virt_cell;
thrust::device_vector<float2>		agents_offset_xy;
thrust::device_vector<unsigned int> agents_ids_copy;
thrust::device_vector<unsigned int>	agents_goal_cell_copy;
thrust::device_vector<unsigned int>	agents_virt_cell_copy;
thrust::device_vector<float2>		agents_offset_xy_copy;
thrust::device_vector<unsigned int> future_occ;
thrust::host_vector<int>			h_seeds;
thrust::device_vector<unsigned int> agents_future_ids;
thrust::device_vector<int>			d_seeds;
thrust::device_vector<float>		agents_pos;
thrust::device_vector<float>		agents_goal;
thrust::device_vector<float>		lca_data;
//
//=======================================================================================
//
class random
{
public:
    int operator() ()
    {
        return rand();
    }
};
//
//=======================================================================================
//
void print_mem( const char* msg, unsigned long long bytes )
{
	std::cout <<
		std::setw( 23 ) << msg <<
		std::setw( 10 ) << bytes / 1024 / 1024 << " MB " << 
		std::setw( 12 ) << bytes << " B" << std::endl;
}
//
//=======================================================================================
//
void print_mem2(	const char*			msg,
					const char*			typ, 
					size_t				tsz, 
					unsigned int		num,
					unsigned long long	bytes )
{
	std::cout << std::setfill( ' ' ) <<
		std::setw( 23 ) << msg <<
		"  " <<
		std::setw( 12 ) << typ << 
		" (" <<
		std::setw(  1 ) << tsz <<
		" B) X " <<
		std::setw(  7 ) << num << 
		" = " <<
		std::setw( 10 ) << bytes << 
		" B (" << 
		std::setw(  3 ) << bytes / 1024 / 1024 << 
		" MB)" << std::endl; 
}
//
//=======================================================================================
//
extern "C" void launch_lca_kernel( std::vector<float>&	pos,
								   std::vector<float>&	goal,
								   std::vector<float>&	lca,
								   float				scene_width,
								   float				scene_height,
								   unsigned int			mesh_width, 
								   unsigned int			mesh_height, 
								   float				time		)
{
//->VIRTUAL_ADVANCE
	cudaEventRecord( start, 0 );
	{
		thrust::copy( agents_ids.begin(), agents_ids.end(), agents_future_ids.begin() );
		advance_agents_virtually(	cells_ids,
									future_occ,
									agents_future_ids,
									agents_ids_copy,
									agents_virt_cell,
									agents_goal_cell,
									SCENE_WIDTH_IN_CELLS	);
	}
	cudaEventRecord( stop, 0 );
	cudaEventSynchronize( stop );
	cudaEventElapsedTime( &temp_time, start, stop );
	virtual_advance_time += temp_time;
	COLLISIONS = count_collisions( future_occ );

#if defined __DEBUG && defined __PRINT_FUTURE_OCC
	std::cout << "Future occupancy[" << COUNTER << "] (natural):" << std::endl;
	for( unsigned int r = 0; r < SCENE_WIDTH_IN_CELLS; r++ )
	{
		for( unsigned int c = 0; c < SCENE_WIDTH_IN_CELLS; c++ )
		{
			std::cout << std::setw( 2 ) << future_occ[r * SCENE_WIDTH_IN_CELLS + c];
		}
		std::cout << std::endl;
	}
	std::cout << "Collisions: " << COLLISIONS << std::endl;
	std::cout << std::endl;
#endif
//<-VIRTUAL_ADVANCE

//->COLLISION_FIX
	cudaEventRecord( start, 0 );
	{
		if( COLLISIONS > 0 )
		{
			fix_collisions(	d_seeds,
							cells_occ,
							future_occ,
							agents_future_ids,
							d_cells_ids,
							agents_ids,
							agents_dids,
							agents_goal_cell,
							agents_virt_cell,
							SCENE_WIDTH_IN_CELLS,
							NUM_AGENTS			);
		}
	}
	cudaEventRecord( stop, 0 );
	cudaEventSynchronize( stop );
	cudaEventElapsedTime( &temp_time, start, stop );
	fix_coll_time += temp_time;
//<-COLLISION_FIX

//->ACTUAL_ADVANCE
	cudaEventRecord( start, 0 );
	{
		advance_agents(	cells_ids,
						cells_occ,
						agents_ids,
						agents_ids_copy,
						agents_goal_cell,
						agents_goal_cell_copy,
						agents_virt_cell,
						agents_virt_cell_copy,
						agents_offset_xy,
						agents_offset_xy_copy,
						agents_pos,
						SCENE_WIDTH_IN_CELLS,
						TILE_WIDTH				);
		thrust::copy( agents_pos.begin(), agents_pos.end(), pos.begin() );
	}
	cudaEventRecord( stop, 0 );
	cudaEventSynchronize( stop );
	cudaEventElapsedTime( &temp_time, start, stop );
	advance_time += temp_time;

#if defined __DEBUG && defined __PRINT_AGENTS
	std::cout << "agents[" << COUNTER << "]" << std::endl;
	for( unsigned int a = 0 ; a < TOTAL_CELLS * DIRECTIONS; a++ )
	{
		if( agents_ids[a] < TOTAL_CELLS * DIRECTIONS )
		{
			float2 axy = agents_offset_xy[a];
			std::cout << "ID: "	<< std::setw( 5 ) << agents_ids[a];
			if( (a / DIRECTIONS) == agents_goal_cell[a] )
			{
				std::cout << " CURR: "	<< std::setw( 3 ) <<  "G";
			}
			else
			{
				std::cout << " CURR: "	<< std::setw( 3 ) <<  (a / DIRECTIONS);
			}
			std::cout << std::fixed <<
				" NEXT: "	<< std::setw( 3 ) << agents_virt_cell[a] <<
				" GOAL: "	<< std::setw( 3 ) << agents_goal_cell[a] <<
				" DX: "		<< std::setw( 6 ) << std::setprecision( 2 ) << axy.x <<
				" DY: "		<< std::setw( 6 ) << std::setprecision( 2 ) << axy.y << std::endl;
		}
	}
	std::cout << std::endl;
#endif

#ifdef __DEBUG
	std::cout << "occupancy[" << COUNTER << "] (fixed):" << std::endl;
	for( unsigned int r = 0; r < SCENE_WIDTH_IN_CELLS; r++ )
	{
		for( unsigned int c = 0; c < SCENE_WIDTH_IN_CELLS; c++ )
		{
			std::cout << std::setw( 2 ) << cells_occ[r * SCENE_WIDTH_IN_CELLS + c];
		}
		std::cout << std::endl;
	}
	COLLISIONS = count_collisions( cells_occ );
	if( COLLISIONS > 0 )
		std::cout << "Collisions: " << COLLISIONS << " (!)" << std::endl;
	else
		std::cout << "Collisions: " << COLLISIONS << std::endl;
	std::cout << std::endl;
	std::cout << "-------------------------------------------------" << std::endl << std::endl;
#endif
//<-ACTUAL_ADVANCE

	COUNTER++;
	system( "pause" );
}
//
//=======================================================================================
//
extern "C" void cleanup_lca( void )
{
	cells_ids.clear();
	d_cells_ids.clear();
	cells_row.clear();
	cells_col.clear();
	cells_occ.clear();
	agents_ids.clear();
	agents_dids.clear();
	agents_goal_cell.clear();
	agents_virt_cell.clear();
	agents_offset_xy.clear();
	agents_ids_copy.clear();
	agents_goal_cell_copy.clear();
	agents_virt_cell_copy.clear();
	agents_offset_xy_copy.clear();
	future_occ.clear();
	h_seeds.clear();
	agents_future_ids.clear();
	d_seeds.clear();
	agents_pos.clear();
	agents_goal.clear();
	lca_data.clear();
}
//
//=======================================================================================
//
extern "C" void init_cells_and_agents(	std::vector<float>&	pos,
										std::vector<float>&	goal,
										std::vector<float>& lca,
										float				scene_width,
										float				scene_height,
										unsigned int		lca_width,
										unsigned int		lca_height,
										unsigned int		num_agents,
										bool&				result		)
{
	result						= true;

	cudaEventCreate( &start );
	cudaEventCreate( &stop  );	

	NUM_AGENTS					= num_agents;
	//SCENE_WIDTH_IN_CELLS		= ( (unsigned int)(3.0 * sqrtf( (float)NUM_AGENTS )) );
	SCENE_WIDTH_IN_CELLS		= lca_width;
	TILE_WIDTH					= scene_width / (float)SCENE_WIDTH_IN_CELLS;
	HALF_SCENE_WIDTH_IN_CELLS	= (float)SCENE_WIDTH_IN_CELLS / 2.0f;
	TOTAL_CELLS					= SCENE_WIDTH_IN_CELLS * SCENE_WIDTH_IN_CELLS;
	virtual_advance_time		= 0.0f;
	fix_coll_time				= 0.0f;
	advance_time				= 0.0f;
	temp_time					= 0.0f;
	COUNTER						= 0;
	COLLISIONS					= 0;

	size_t reserved, total;
	unsigned long long	required,					future_occ_size, 
						cells_ids_size,				d_cells_ids_size,
						cells_occ_size,				agents_ids_size,
						agents_dids_size,			agents_goal_cell_size,
						agents_virt_cell_size,		agents_offset_xy_size,
						agents_ids_copy_size,		seeds_size,
						agents_goal_cell_copy_size,	agents_virt_cell_copy_size,
						agents_offset_xy_copy_size,	agents_future_ids_size,
						lca_data_size;

	cudaMemGetInfo( &reserved, &total );
	future_occ_size				= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS					);
	cells_ids_size				= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS					);
	d_cells_ids_size			= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	cells_occ_size				= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS					);
	agents_ids_size				= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_dids_size			= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_goal_cell_size		= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_virt_cell_size		= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_offset_xy_size		= (unsigned long long) sizeof( float2		) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_ids_copy_size		= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_goal_cell_copy_size	= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_virt_cell_copy_size	= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_offset_xy_copy_size	= (unsigned long long) sizeof( float2		) * (TOTAL_CELLS	* DIRECTIONS	);
	seeds_size					= (unsigned long long) sizeof( int			) * (TOTAL_CELLS	* DIRECTIONS	);
	agents_future_ids_size		= (unsigned long long) sizeof( unsigned int	) * (TOTAL_CELLS	* DIRECTIONS	);
	lca_data_size				= (unsigned long long) sizeof( float		) * (TOTAL_CELLS					);
	required					=	future_occ_size				+ 
									cells_ids_size				+
									d_cells_ids_size			+
									cells_occ_size				+
									agents_ids_size				+
									agents_dids_size			+
									agents_goal_cell_size		+
									agents_virt_cell_size		+
									agents_offset_xy_size		+
									agents_ids_copy_size		+
									agents_goal_cell_copy_size	+
									agents_virt_cell_copy_size	+
									agents_offset_xy_copy_size	+
									seeds_size					+
									agents_future_ids_size		+
									lca_data_size;
	std::cout << std::endl;
	std::cout << "NUM_AGENTS:\t\t" << num_agents << std::endl;
	std::cout << "SCENE_WIDTH:\t\t" << scene_width << std::endl;
	std::cout << "SCENE_WIDTH_IN_CELLS:\t" << SCENE_WIDTH_IN_CELLS << std::endl;
	std::cout << "TILE_WIDTH:\t\t" << TILE_WIDTH << std::endl;
	std::cout << "TOTAL_CELLS:\t\t" << SCENE_WIDTH_IN_CELLS * SCENE_WIDTH_IN_CELLS << std::endl;
	
	std::cout << std::endl;
	std::cout << "-------------------------------------------------" << std::endl;
	print_mem( "Total memory",			(unsigned long long) total		);
	print_mem( "Reserved memory",		(unsigned long long) reserved	);
	std::cout << "-------------------------------------------------" << std::endl;
	print_mem2( "Random seeds",				"int",			sizeof(int),			TOTAL_CELLS * DIRECTIONS,	seeds_size				);
	print_mem2( "Cells ids",				"unsigned int", sizeof(unsigned int),	TOTAL_CELLS,				cells_ids_size			);
	print_mem2( "D cells ids",				"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	d_cells_ids_size		);
	print_mem2( "Cells occ",				"unsigned int", sizeof(unsigned int),	TOTAL_CELLS,				cells_occ_size			);
	print_mem2( "Future Occupancy",			"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	future_occ_size			);
	print_mem2( "Agents ids",				"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_ids_size			);
	print_mem2( "Agents dids",				"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_ids_size			);
	print_mem2( "Agents ids copy",			"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_ids_copy_size	);
	print_mem2( "Agents future ids",		"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_future_ids_size	);
	print_mem2( "Agents goal cell",			"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_goal_cell_size	);
	print_mem2( "Agents goal cell copy",	"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_goal_cell_size	);
	print_mem2( "Agents virt cell",			"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_virt_cell_size	);
	print_mem2( "Agents virt cell copy",	"unsigned int", sizeof(unsigned int),	TOTAL_CELLS * DIRECTIONS,	agents_virt_cell_size	);
	print_mem2( "Agents xy",				"float2",		sizeof(float2),			TOTAL_CELLS * DIRECTIONS,	agents_offset_xy_size	);
	print_mem2( "Agents xy copy",			"float2",		sizeof(float2),			TOTAL_CELLS * DIRECTIONS,	agents_offset_xy_size	);
	print_mem2( "LCA data",					"float",		sizeof(float),			TOTAL_CELLS,				lca_data_size			);
	std::cout << "-------------------------------------------------" << std::endl;
	print_mem( "Required memory",		required						);
	if( required > (unsigned long long)reserved )
	{
		std::cout << std::endl;
		std::cout << "Not enough memory!" << std::endl << std::endl;
		result = false;
		return;
	}
	std::cout << std::endl;

	cudaEventRecord( start, 0 );
	{
		cells_ids.resize			( TOTAL_CELLS				);
		d_cells_ids.resize			( TOTAL_CELLS * DIRECTIONS	);
		cells_row.resize			( TOTAL_CELLS				);
		cells_col.resize			( TOTAL_CELLS				);
		cells_occ.resize			( TOTAL_CELLS				);
		agents_ids.resize			( TOTAL_CELLS * DIRECTIONS	);
		agents_dids.resize			( TOTAL_CELLS * DIRECTIONS	);
		agents_goal_cell.resize		( TOTAL_CELLS * DIRECTIONS	);
		agents_virt_cell.resize		( TOTAL_CELLS * DIRECTIONS	);
		agents_offset_xy.resize		( TOTAL_CELLS * DIRECTIONS	);
		agents_ids_copy.resize		( TOTAL_CELLS * DIRECTIONS	);
		agents_goal_cell_copy.resize( TOTAL_CELLS * DIRECTIONS	);
		agents_virt_cell_copy.resize( TOTAL_CELLS * DIRECTIONS	);
		agents_offset_xy_copy.resize( TOTAL_CELLS * DIRECTIONS	);
		future_occ.resize			( TOTAL_CELLS				);
		h_seeds.resize				( TOTAL_CELLS * DIRECTIONS	);
		agents_future_ids.resize	( TOTAL_CELLS * DIRECTIONS	);
		lca_data.resize				( TOTAL_CELLS				);

		thrust::generate( h_seeds.begin(), h_seeds.end(), random() );
		d_seeds			= h_seeds;

		agents_pos		= pos;
		agents_goal		= goal;
		lca_data		= lca;
	}
	cudaEventRecord( stop, 0 );
	cudaEventSynchronize( stop );
	cudaEventElapsedTime( &mem_alloc_time, start, stop );

	cudaEventRecord( start, 0 );
	{
		init_cells_and_agents(	agents_pos,
								agents_goal,
								lca_data,
								d_seeds,
								cells_ids,
								d_cells_ids,
								cells_occ,
								agents_ids,
								agents_dids,
								agents_goal_cell,
								agents_virt_cell,
								agents_offset_xy,
								HALF_SCENE_WIDTH_IN_CELLS,
								TILE_WIDTH,
								SCENE_WIDTH_IN_CELLS,
								NUM_AGENTS				);
	}
	cudaEventRecord( stop, 0 );
	cudaEventSynchronize( stop );
	cudaEventElapsedTime( &agents_time, start, stop );

#if defined __DEBUG && defined __PRINT_AGENTS
	std::cout << "Agents:\n";
	for( unsigned int a = 0 ; a < TOTAL_CELLS * DIRECTIONS; a++ )
	{
		if( agents_ids[a] < TOTAL_CELLS * DIRECTIONS )
		{
			float2 axy = agents_offset_xy[a];
			std::cout << "ID: "	<< std::setw( 5 ) << agents_ids[a];
			if( (a / DIRECTIONS) == agents_goal_cell[a] )
			{
				std::cout << " CURR: "	<< std::setw( 3 ) <<  "G";
			}
			else
			{
				std::cout << " CURR: "	<< std::setw( 3 ) <<  (a / DIRECTIONS);
			}
			std::cout << std::fixed <<
				" NEXT: "	<< std::setw( 3 ) << agents_virt_cell[a] <<
				" GOAL: "	<< std::setw( 3 ) << agents_goal_cell[a] <<
				" DX: "		<< std::setw( 6 ) << std::setprecision( 2 ) << axy.x <<
				" DY: "		<< std::setw( 6 ) << std::setprecision( 2 ) << axy.y << std::endl;
		}
	}
	std::cout << std::endl;
#endif

#ifdef __DEBUG
	std::cout << "Initial occupancy:\n";
	for( unsigned int r = 0; r < SCENE_WIDTH_IN_CELLS; r++ )
	{
		for( unsigned int c = 0; c < SCENE_WIDTH_IN_CELLS; c++ )
		{
			std::cout << std::setw( 2 ) << cells_occ[r * SCENE_WIDTH_IN_CELLS + c];
		}
		std::cout << std::endl;
	}
	COLLISIONS = count_collisions( cells_occ );
	std::cout << "Collisions: " << COLLISIONS << std::endl;
	std::cout << std::endl;
	std::cout << "-------------------------------------------------" << std::endl << std::endl;
#endif

	total_time	= mem_alloc_time + agents_time;
	//std::cout << std::fixed	<< std::setfill( '0' ) << std::endl <<
	std::cout << std::endl <<
	"TIMES:                "																					<< std::endl <<
	"MEM ALLOC:            " << std::setw( 10 ) << std::setprecision( 6 ) << mem_alloc_time			<< " ms."	<< std::endl <<
	"AGENTS:               " << std::setw( 10 ) << std::setprecision( 6 ) << agents_time			<< " ms."	<< std::endl <<
	"----------------------------"																				<< std::endl <<
	"TOTAL:                " << std::setw( 10 ) << std::setprecision( 6 ) << total_time				<< " ms."	<< std::endl <<
	"----------------------------"																				<< std::endl <<
	std::endl;
}
//
//=======================================================================================
#endif
