// @REV_TAG SRL_09_2011

#pragma once
#include "cTextureManager.h"
#include "cGlslManager.h"
#include "cVboManager.h"
#include "glew.h"
#define _USE_MATH_DEFINES 1
#include <math.h>
#include <stdio.h>
#include <vector>

using namespace std;

//=======================================================================================

#ifndef DEG2RAD
	#define DEG2RAD	0.01745329251994329576f
#endif  DEG2RAD

//=======================================================================================

#ifndef RAD2DEG
	#define RAD2DEG	57.29577951308232087679f
#endif  RAD2DEG

//=======================================================================================

#ifndef __GL_ERROR_MANAGER
#define __GL_ERROR_MANAGER

class GlErrorManager
{
public:
				GlErrorManager	( void						);
				~GlErrorManager	( void						);

	char*		getString		( unsigned int	err			);
	GLenum		getError		( void						);
	GLenum		getError		( char*			note		);
};

#endif __GL_ERROR_MANAGER

//=======================================================================================

#ifndef __VECTOR3F
#define __VECTOR3F

class vector3f
{
public:
					vector3f			( void						);
					vector3f			( float		x, 
										  float		y, 
										  float		z				);
					vector3f			( float*	components		);
					~vector3f			( void						);

	void			clear				( void						);
    void			set					( float		x, 
										  float		y, 
										  float		z				);
	void			set					( vector3f& v				);
	void			set					( float*	components		);
	void			set_and_normalize	( float		x, 
										  float		y, 
										  float z					);
	void			set_and_normalize	( vector3f& v				);
	void			set_and_normalize	( float*	components		);
    float			getNorm				( void						);
    void			normalize			( void						);
    float			distance			( vector3f& v				);
    float			dotProduct			( vector3f& v				);
    vector3f		crossProduct		( vector3f& v				);
	vector<float>	asVector			( void						);
	float*			asArray				( void						);

    vector3f		operator +	(const vector3f& other);
    vector3f		operator -	(const vector3f& other);
    vector3f		operator *	(const vector3f& other);
	vector3f		operator *	(const float scalar);
    vector3f		operator /	(const vector3f& other);
    vector3f&		operator =	(const vector3f& other);
    vector3f&		operator += (const vector3f& other);
    vector3f&		operator -= (const vector3f& other);
    vector3f		operator -	(void) const;
	// Length comparison <:
    friend bool operator <	(const vector3f& me, const vector3f& other)
	{
		float  val1 = me.x    * me.x    + me.y    * me.y    + me.z    * me.z;
		float  val2 = other.x * other.x + other.y * other.y + other.z * other.z;
		return val1 < val2;
	}
	// Length comparison >:
    friend bool operator >	(const vector3f& me, const vector3f& other)
	{
		float  val1 = me.x    * me.x    + me.y    * me.y    + me.z    * me.z;
		float  val2 = other.x * other.x + other.y * other.y + other.z * other.z;
		return val1 > val2;
	}
    float			x;
    float			y;
    float			z;
	float arr[3];
};

#endif __VECTOR3F

//=======================================================================================

#ifndef __FRUSTUM_PLANE
#define __FRUSTUM_PLANE

class FrustumPlane
{
public:
	FrustumPlane( vector3f& v1, vector3f& v2, vector3f& v3 )
	{
		set3Points( v1, v2, v3 );
	};
	FrustumPlane(){};
	~FrustumPlane(){};

	void set3Points( vector3f& v1, vector3f& v2, vector3f& v3 )
	{
		vector3f A( v3.x - v2.x, v3.y - v2.y, v3.z - v2.z );
		vector3f B( v1.x - v2.x, v1.y - v2.y, v1.z - v2.z );
		N = A * B;
		N.normalize();
		P.set( v2 );
		D = -N.dotProduct( P );
	}

	void setNormalAndPoint( vector3f& normal, vector3f& point )
	{
		N.set( normal );
		N.normalize();
		P.set( point );
		D = -N.dotProduct( P );
	}

	void setCoefficients( float a, float b, float c, float d )
	{
		N.set( a, b, c );
		float len	= N.getNorm();
		N.set( a / len, b / len, c / len );
		D			= d / len;
	}

	float distance( vector3f &p )
	{
		return (D + N.dotProduct( p ));
	}

public:
	vector3f	N;
	vector3f	P;
	float		D;
};
#endif __FRUSTUM_PLANE

//=======================================================================================

#ifndef __PROJECTION_MANAGER
#define __PROJECTION_MANAGER

class ProjectionManager
{
public:
	ProjectionManager( void );
	~ProjectionManager( void );

	void		setOrthoProjection( unsigned int	w, 
									unsigned int	h, 
									bool			backup_viewport = true );
	void		setOrthoProjection( unsigned int	x, 
									unsigned int	y,
									unsigned int	w,
									unsigned int	h,
									unsigned int	l,
									unsigned int	r,
									unsigned int	b,
									unsigned int	t,
									bool			backup_viewport = true );
	void		setTextProjection( unsigned int w, unsigned int h );
	void		restoreProjection( void );
	GLint*		getViewport( void );
	float		getAspectRatio( void );
public:
	static enum { ORTHOGRAPHIC, TEXT };
private:
	float		aspect_ratio;
	GLint		viewport[4];
	bool		backup_viewport;
	int			type;
};

#endif __PROJECTION_MANAGER

//=======================================================================================

#ifndef __FRUSTUM
#define __FRUSTUM

class Frustum
{
public:
						Frustum( int type, float fov, float nearD, float farD );
						~Frustum( void );

	static enum			{ NONE, GEOMETRIC, RADAR };
	static enum			{ OUTSIDE, INTERSECT, INSIDE };
	int					pointInFrustum( vector3f& p );
	int					sphereInFrustum( vector3f& p, float radius );
	int					boxInFrustum( vector3f& box_center, vector3f& box_halfdiag );
	void				setPlanes( vector3f& P, vector3f& L, vector3f& U );
	void				setFovY( float fov );
	void				updateRatio( void );
	void				setNearD( float nearD );
	void				setFarD( float farD );
	void				drawPoints( void );
	void				drawLines( void );
	void				drawPlanes( void );
	void				drawNormals( void );
	unsigned int		getCulledCount( void );
	void				incCulledCount( void );
	void				resetCulledCount( void );
	float				getNearD( void );
	float				getFarD( void );
	float				getFovY( void );
	float				getNearW( void );
	float				getNearH( void );
	float				getFarW( void );
	float				getFarH( void );
	float				getRatio( void );
	vector3f&			getX( void );
	vector3f&			getY( void );
	vector3f&			getZ( void );
	vector3f			X;
	vector3f			Y;
	vector3f			Z;
	float				RATIO;
	float				TANG;

private:
	static enum			{ TOP, BOTTOM, LEFT, RIGHT, NEARP, FARP };
	unsigned int		culled_count;
	int					vfc_type;
	vector3f			NTL;	// NEAR_TOP_LEFT
	vector3f			NTR;	// NEAR_TOP_RIGHT
	vector3f			NBL;	// NEAR_BOTTOM_LEFT
	vector3f			NBR;	// NEAR_BOTTOM_RIGHT
	vector3f			FTL;	// FAR_TOP_LEFT
	vector3f			FTR;	// FAR_TOP_RIGHT
	vector3f			FBL;	// FAR_BOTTOM_LEFT
	vector3f			FBR;	// FAR_BOTTOM_RIGHT
	vector3f			CAM_POS;
	vector3f			near_center;
	vector3f			far_center;
	float				near_distance;
	float				far_distance;
	float				FOVY;
	float				near_width;
	float				near_height;
	float				far_width;
	float				far_height;
	float				sphereFactorX;
	float				sphereFactorY;
	float				fR;
	float				fG;
	float				fB;
	FrustumPlane		frustum_planes[6];
	ProjectionManager*	proj_man;
};

#endif __FRUSTUM

//=======================================================================================

#ifndef	__SKYBOX_MANAGER
#define __SKYBOX_MANAGER

class SkyboxManager
{
public:
									SkyboxManager			( unsigned int		id,
															  GlslManager*		shader_manager, 
															  VboManager*		vbo_manager,
															  vector3f&			center,
															  vector3f&			extents,
															  vector<bool>&		bump, 
															  vector<float>&	tile,
															  bool				instancing,
															  bool				lighting		);
									~SkyboxManager			( void								);

	bool							LoadSkyboxTextures		( string&			FRONT_Filename, 
															  unsigned int		front_env,
															  string&			BACK_Filename, 
															  unsigned int		back_env,
															  string&			LEFT_Filename, 
															  unsigned int		left_env,
															  string&			RIGHT_Filename, 
															  unsigned int		right_env,
															  string&			TOP_Filename, 
															  unsigned int		top_env,
															  string&			BOTTOM_Filename,
															  unsigned int		bottom_env		);
	bool							LoadSkyboxBumpTextures	( string&			FRONT_Filename, 
															  unsigned int		front_env,
															  string&			BACK_Filename, 
															  unsigned int		back_env,
															  string&			LEFT_Filename, 
															  unsigned int		left_env,
															  string&			RIGHT_Filename, 
															  unsigned int		right_env,
															  string&			TOP_Filename, 
															  unsigned int		top_env,
															  string&			BOTTOM_Filename,
															  unsigned int		bottom_env		);
	bool							LoadSkyboxSpecTextures	( string&			FRONT_Filename, 
															  unsigned int		front_env,
															  string&			BACK_Filename, 
															  unsigned int		back_env,
															  string&			LEFT_Filename, 
															  unsigned int		left_env,
															  string&			RIGHT_Filename, 
															  unsigned int		right_env,
															  string&			TOP_Filename, 
															  unsigned int		top_env,
															  string&			BOTTOM_Filename,
															  unsigned int		bottom_env		);
	void							draw					( Frustum*			frustum, 
															  bool				bump_enabled, 
															  bool				draw_bv			);
	unsigned int					getVertexCount			( int				face			);
	unsigned int					getTextureWeight		( int				face			);
public:
	static enum						{
										FRONT,
										BACK,
										LEFT,
										RIGHT,
										TOP,
										BOTTOM
									};
private:
	typedef struct
	{
		vector3f					center;
		vector3f					halfdiag;
		float						bR;
		float						bG;
		float						bB;
	}								WallBox;
	GLuint							DIFFUSE_IDS[6];
	GLuint							BUMPMAP_IDS[6];
	GLuint							SPECMAP_IDS[6];
	vector<bool>					bumped;
	vector<float>					tiling;
	bool							instancing;
	bool							lighting;
	vector<float>					front_positions;
	vector<float>					front_rotations;
	vector<float>					back_positions;
	vector<float>					back_rotations;
	vector<float>					left_positions;
	vector<float>					left_rotations;
	vector<float>					right_positions;
	vector<float>					right_rotations;
	vector<float>					top_positions;
	vector<float>					top_rotations;
	vector<float>					bottom_positions;
	vector<float>					bottom_rotations;
	vector<GLuint>					vboIds;
	vector<GLuint>					ivboIds;
	vector<unsigned int>			vbo_indices;
	vector<Vertex>					v_front;
	vector<Vertex>					v_back;
	vector<Vertex>					v_left;
	vector<Vertex>					v_right;
	vector<Vertex>					v_top;
	vector<Vertex>					v_bottom;
	unsigned int					id;
	unsigned int					vertexCounts[6];
	unsigned int					texture_weights[6];
	unsigned int					texture_bump_weights[6];
	unsigned int					texture_spec_weights[6];
	GlslManager*					shader_manager;
	GlErrorManager*					err_manager;
	VboManager*						vbo_manager;
	vector<WallBox>					front_boxes;
	vector<WallBox>					back_boxes;
	vector<WallBox>					left_boxes;
	vector<WallBox>					right_boxes;
	vector<WallBox>					top_boxes;
	vector<WallBox>					bottom_boxes;
	GLuint							skyboxList;
	float							WIDTH;
	float							HWIDTH;
	float							HEIGHT;
	float							HHEIGHT;
	float							LENGTH;
	float							HLENGTH;
	float							front_tile_width;
	float							front_tile_height;
	float							back_tile_width;
	float							back_tile_height;
	float							left_tile_width;
	float							left_tile_height;
	float							right_tile_width;
	float							right_tile_height;
	float							top_tile_width;
	float							top_tile_height;
	float							bottom_tile_width;
	float							bottom_tile_height;
	vector3f						center;
private:
	void							init					( void								);
	void							gen_vbos				( void								);
	void							fill_vertex				( Vertex&			v,
															  float				loc0, 
															  float				loc1, 
															  float				loc2, 
															  float				nor0, 
															  float				nor1, 
															  float				nor2, 
															  float				tex0, 
															  float				tex1			);
	void							fill_tangents			( Vertex&			vert1, 
															  Vertex&			vert2, 
															  Vertex&			vert3, 
															  Vertex&			vert4			);
	vector3f						findTangent				( Vertex&			v1, 
															  Vertex&			v2, 
															  Vertex&			v3				);
	void							renderSkyboxBVs			( void								);
	void							drawBVs					( void								);
	bool							tilesFrustumCheck		( Frustum*			frustum, 
															  vector<WallBox>&	boxes			);
};

#endif __SKYBOX_MANAGER

//=======================================================================================
