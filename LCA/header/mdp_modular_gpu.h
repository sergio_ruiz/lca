// @REV_TAG SRL_09_2013
//
//																		 VBO MANAGER v1.0
//													 Sergio Ruiz Loza. A01106919@itesm.mx
//																	  All Rights Reserved
//													  ITESM-CCM (http://www.ccm.itesm.mx)
//												 Computer Science Ph.D. Thesis Subproject
//										   Global Thesis Project: "Crowd Motion Planning"
//							Thesis Director: Dr. Benjam�n Hern�ndez A. hbenjamin@itesm.mx
//											   Programmed in C++ for OpenGL 3.1 and newer
//  														   Requires GLEW and FREEGLUT
//
//
//															   See ReadMe.txt for details
//
//=======================================================================================

#pragma once

#include <vector>

void mmdp_init_permutations_on_device	(	const int				_rows,
											const int				_columns,
											const int				_NQ,
											std::vector<int>&		host_dir_ib_iwNQ,
											std::vector<int>&		host_dir_ib_iwNQ_inv,
											std::vector<float>&		host_probability1,
											std::vector<float>&		host_probability2,
											std::vector<float>&		host_permutations		);

void mmdp_upload_to_device				(	std::vector<int>&		P,
											std::vector<float>&		Q,
											std::vector<float>&		V,
											std::vector<int>&		host_vicinityNQ,
											std::vector<float>&		host_dir_rwNQ,
											std::vector<float>&		host_dir_pvNQ,
											std::vector<float>&		host_permutations		);

int mmdp_iterate_on_device				(	float					discount,
											bool&					convergence				);

void mmdp_download_to_host				(	std::vector<int>&		P,
											std::vector<float>&		V						);
