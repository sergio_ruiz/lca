// @REV_TAG SRL_03_2014
//
//													 Sergio Ruiz Loza. A01106919@itesm.mx
//																	  All Rights Reserved
//													  ITESM-CCM (http://www.ccm.itesm.mx)
//												 Computer Science Ph.D. Thesis Subproject
//										   Global Thesis Project: "Crowd Motion Planning"
//							Thesis Director: Dr. Benjamín Hernández A. hbenjamin@itesm.mx
//											   Programmed in C++ for OpenGL 3.1 and newer
//  														   Requires GLEW and FREEGLUT
//
//
//															   See ReadMe.txt for details
//
//=======================================================================================

#pragma once

#ifdef _WIN32
	#define WINDOWS_LEAN_AND_MEAN
	#define NOMINMAX
	#include <windows.h>
#endif

#include <cuda_runtime.h>
#include <math.h>

#include "cMacros.h"
#include "cVertex.h"
#include "cLogManager.h"
#include "cVboManager.h"
#include "cStaticLod.h"
#include "cGlErrorManager.h"

//=======================================================================================

#ifndef __LCA_CUDA_PATH_MANAGER
#define __LCA_CUDA_PATH_MANAGER

class LCACudaPathManager
{
public:
								LCACudaPathManager		(	LogManager*			log_manager,
															VboManager*			vbo_manager			);
								~LCACudaPathManager		(	void									);

	bool						init					(	float				_scene_width,
															float				_scene_depth,
															unsigned int		_mdp_width,
															unsigned int		_mdp_depth,
															unsigned int		_lca_width,
															unsigned int		_lca_depth,
															vector<float>&		_mdp_policy,
															vector<float>&		_init_pos,
															vector<float>&		_goals				);
	void						runCuda					(	unsigned int		texture_width,
															unsigned int		texture_height,
															float				parameter			);

	unsigned int				cuda_pos_vbo_id;
	unsigned int				cuda_pos_vbo_size;
	unsigned int				cuda_pos_vbo_index;
	unsigned int				cuda_pos_vbo_frame;

	unsigned int				cuda_goals_vbo_id;
	unsigned int				cuda_goals_vbo_size;
	unsigned int				cuda_goals_vbo_index;
	unsigned int				cuda_goals_vbo_frame;

	GLuint						pos_tbo_id;
	vector<float>				positions;
	vector<float>				goals;

private:
	bool						init					(	void									);
	bool						setLCAExits				(	void									);
	int							getMaxGflopsDeviceId	(	void									);
	int							_ConvertSMVer2Cores		(	int					major,
															int					minor				);

	LogManager*					log_manager;
	VboManager*					vbo_manager;

	vector<float>				mdp_policy;
	vector<float>				lca_exits;

	float						scene_width;
	float						scene_depth;

	unsigned int				lca_cells_in_mdp_cell;
	unsigned int				lca_in_mdp_width;
	unsigned int				lca_in_mdp_depth;

	unsigned int				mdp_width;
	unsigned int				mdp_depth;

	unsigned int				lca_width;
	unsigned int				lca_depth;
};

#endif

//=======================================================================================
