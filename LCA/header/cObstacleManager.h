#pragma once

#include "cMDPSquareManager.h"
#include "cLogManager.h"

#include <vector>
#include <string>

using namespace std;

#ifndef __OBSTACLE_MANAGER
#define __OBSTACLE_MANAGER

class ObstacleManager
{
public:
							ObstacleManager			(	LogManager*		_log_manager,
														float			_scene_width,
														float			_scene_depth			);
							~ObstacleManager		(	void									);

	bool					init					(	string&			_mdp_csv_file			);
	vector<float>&			getMDPPolicy			(	void									);
	unsigned int			getMDPSceneWidthInTiles	(	void									);
	unsigned int			getMDPSceneDepthInTiles	(	void									);

private:
	bool					initMDP					(	void									);

	LogManager*				log_manager;

	unsigned int			mdp_scene_width_in_tiles;
	unsigned int			mdp_scene_depth_in_tiles;
	float					scene_width;
	float					scene_depth;
	float					mdp_tile_width;
	float					mdp_tile_depth;

	unsigned int			NQ;
	MDPSquareManager*		mdp_manager;
	string					mdp_csv_file;
	vector<float>			mdp_policy;
	vector<float>			mdp_topology;
};

#endif
