// @REV_TAG SRL_03_2014
//
//													 Sergio Ruiz Loza. A01106919@itesm.mx
//																	  All Rights Reserved
//													  ITESM-CCM (http://www.ccm.itesm.mx)
//												 Computer Science Ph.D. Thesis Subproject
//										   Global Thesis Project: "Crowd Motion Planning"
//							Thesis Director: Dr. Benjam�n Hern�ndez A. hbenjamin@itesm.mx
//											   Programmed in C++ for OpenGL 3.1 and newer
//  														   Requires GLEW and FREEGLUT
//
//
//															   See ReadMe.txt for details
//
//=======================================================================================

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include <thrust/random.h>
#include <thrust/inner_product.h>
#include <thrust/binary_search.h>
#include <thrust/adjacent_difference.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/iterator/counting_iterator.h>
#include <thrust/tabulate.h>
#include <thrust/transform.h>
#include <thrust/transform_scan.h>
#include <thrust/reduce.h>
#include <iostream>
#include <iomanip>
#include <bitset>
#include <algorithm>
#include <math.h>
#include <sstream>
#include <time.h>
#include "ccThrustUtil.h"

#ifndef DEG2RAD
	#define DEG2RAD	0.01745329251994329576f
#endif  DEG2RAD

#ifndef RAD2DEG
	#define RAD2DEG	57.29577951308232087679f
#endif  RAD2DEG

#ifndef DIRECTIONS
	#define DIRECTIONS	8
#endif

#define SIGNF( X ) (X < -0.000001f) ? -1.0f : ((X > 0.000001f) ? 1.0f : 0.0f)
#define INBOUNDS( X, LIMIT ) X > -1 && X < LIMIT

thrust::minus<int>										minus_i;
thrust::multiplies<int>									mult_i;
thrust::minus<unsigned int>								minus_ui;
typedef thrust::device_vector<unsigned int>::iterator	UI_Iterator;
//
//=======================================================================================
//
unsigned int find_npot( unsigned int side )
{
	unsigned int npot = side;
	//http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
	npot--;
	npot |= npot >> 1;
	npot |= npot >> 2;
	npot |= npot >> 4;
	npot |= npot >> 8;
	npot |= npot >> 16;
	npot++;
	return npot;
}
//
//=======================================================================================
//
__host__ __device__ unsigned int hash( unsigned int x )
{
	x = (x+0x7ed55d16) + (x<<12);
	x = (x^0xc761c23c) ^ (x>>19);
	x = (x+0x165667b1) + (x<<5);
	x = (x+0xd3a2646c) ^ (x<<9);
	x = (x+0xfd7046c5) + (x<<3);
	x = (x^0xb55a4f09) ^ (x>>16);
	return x;
}
//
//=======================================================================================
//
struct advance_n_agents
{
	unsigned int	scene_width_in_cells;
	unsigned int	total_cells;
	unsigned int	tcd;

	unsigned int*	raw_agents_ids_ptr;
	unsigned int*	raw_agents_ids_copy_ptr;
	unsigned int*	raw_goal_cells_ptr;
	unsigned int*	raw_goal_cells_copy_ptr;
	unsigned int*	raw_virt_cells_ptr;
	unsigned int*	raw_virt_cells_copy_ptr;

	float2*			raw_agents_xy_ptr;
	float2*			raw_agents_xy_copy_ptr;

	float*			raw_agents_pos_ptr;

	float			cell_width;
	float			fhsw;
	float			f_deg_inc;
	
	int				swic;

	advance_n_agents(	float			_cell_width,
						unsigned int	_swic,
						unsigned int*	_raip,
						unsigned int*	_raicp,
						unsigned int*	_rgcp,
						unsigned int*	_rgccp,
						unsigned int*	_rvcp,
						unsigned int*	_rvccp,
						float2*			_raxyp,
						float2*			_raxycp,
						float*			_rapp	) : cell_width				(	_cell_width	),
													scene_width_in_cells	(	_swic		),
													raw_agents_ids_ptr		(	_raip		),
													raw_agents_ids_copy_ptr	(	_raicp		),
													raw_goal_cells_ptr		(	_rgcp		),
													raw_goal_cells_copy_ptr	(	_rgccp		),
													raw_virt_cells_ptr		(	_rvcp		),
													raw_virt_cells_copy_ptr	(	_rvccp		),
													raw_agents_xy_ptr		(	_raxyp		),
													raw_agents_xy_copy_ptr	(	_raxycp		),
													raw_agents_pos_ptr		(	_rapp		)
	{
		total_cells = scene_width_in_cells * scene_width_in_cells;
		tcd			= total_cells * DIRECTIONS;
		f_deg_inc	= 360.0f / (float)DIRECTIONS;
		swic		= (int)scene_width_in_cells;
		fhsw		= ((float)scene_width_in_cells * cell_width) / 2.0f;
	}

	template <typename Tuple>						//0        1
	__host__ __device__ void operator()( Tuple t )	//CELL_ID, OCCUPANCY
	{
		unsigned int	cell_id				= thrust::get<0>(t);
		unsigned int	neighbor_cell_id	= total_cells;
		unsigned int	offset				= 0;
		unsigned int	occupancy			= 0;
		unsigned int	cda					= 0;
		unsigned int	nda					= 0;
		unsigned int	cdo					= 0;
		unsigned int	cd					= cell_id * DIRECTIONS;
		unsigned int	d					= 0;
		unsigned int	a					= 0;
		int				row					= (int)(cell_id / scene_width_in_cells);
		int				col					= (int)(cell_id % scene_width_in_cells);
		int				test_neighbor_col	= -1;
		int				test_neighbor_row	= -1;
		float			test_radians		= 0.0f;

		for( a = 0; a < DIRECTIONS; a++ )	//STAYING
		{
			cda = cd + a;
			if( raw_virt_cells_copy_ptr[cda] == tcd )
			{
				break;
			}
			if( raw_virt_cells_copy_ptr[cda] == cell_id )	//ACTUALLY_STAYING
			{
				cdo							= cd + offset;
				raw_agents_ids_ptr[cdo]		= raw_agents_ids_copy_ptr[cda];
				raw_goal_cells_ptr[cdo]		= raw_goal_cells_copy_ptr[cda];
				raw_agents_xy_ptr[cdo]		= raw_agents_xy_copy_ptr[cda];

				int				gx			= (int)(raw_goal_cells_copy_ptr[cda] % scene_width_in_cells);
				int				gy			= (int)(raw_goal_cells_copy_ptr[cda] / scene_width_in_cells);
				int				diffX		= (gx - col);
				int				ncx			= diffX;
				if( ncx != 0 )	ncx			= diffX / abs( diffX );
				int				diffY		= (gy - row);
				int				ncy			= diffY;
				if( ncy != 0 )	ncy			= diffY / abs( diffY );
				unsigned int	ntx			= (unsigned int)(col + ncx);
				unsigned int	nty			= (unsigned int)(row + ncy);
				raw_virt_cells_ptr[cdo]		= nty * scene_width_in_cells + ntx;

				unsigned int	aid			= raw_agents_ids_copy_ptr[cda] * 4;
				float			adx			= raw_agents_xy_copy_ptr[cda].x;
				float			adz			= raw_agents_xy_copy_ptr[cda].y;
				float			ax			= (((float)col * cell_width) + adx) - fhsw;
				float			az			= (((float)row * cell_width) + adz) - fhsw;
				raw_agents_pos_ptr[aid + 0]	= ax;
				raw_agents_pos_ptr[aid + 2] = az;

				if( cell_id != raw_goal_cells_copy_ptr[cda] )
				{
					occupancy++;
				}
				offset++;
			}
		}

		for( d = 0; d < DIRECTIONS; d++ )	//INCOMING
		{
			test_radians		= DEG2RAD * ((float)d * f_deg_inc);
			test_neighbor_col	= col + ((int)SIGNF(  cosf( test_radians ) ));
			test_neighbor_row	= row + ((int)SIGNF( -sinf( test_radians ) ));
			if( INBOUNDS( test_neighbor_col, swic ) && 
				INBOUNDS( test_neighbor_row, swic )		)
			{
				neighbor_cell_id = (unsigned int)(test_neighbor_row * swic + test_neighbor_col);
				for( a = 0; a < DIRECTIONS; a++ )
				{
					nda = neighbor_cell_id * DIRECTIONS + a;
					if( raw_virt_cells_copy_ptr[nda] == tcd )
					{
						break;
					}
					if( raw_virt_cells_copy_ptr[nda] == cell_id )	//INCOMING_HERE
					{
						cdo							= cd + offset;
						raw_agents_ids_ptr[cdo]		= raw_agents_ids_copy_ptr[nda];
						raw_goal_cells_ptr[cdo]		= raw_goal_cells_copy_ptr[nda];

						int				gx			= (int)(raw_goal_cells_copy_ptr[nda] % scene_width_in_cells);
						int				gy			= (int)(raw_goal_cells_copy_ptr[nda] / scene_width_in_cells);
						int				diffX		= (gx - col);
						int				ncx			= diffX;
						if( ncx != 0 )	ncx			= diffX / abs( diffX );
						int				diffY		= (gy - row);
						int				ncy			= diffY;
						if( ncy != 0 )	ncy			= diffY / abs( diffY );
						unsigned int	ntx			= (unsigned int)(col + ncx);
						unsigned int	nty			= (unsigned int)(row + ncy);

						raw_virt_cells_ptr[cdo]		= nty * scene_width_in_cells + ntx;
						raw_agents_xy_ptr[cdo]		= raw_agents_xy_copy_ptr[nda];

						unsigned int	aid			= raw_agents_ids_copy_ptr[nda] * 4;
						float			adx			= raw_agents_xy_copy_ptr[nda].x;
						float			adz			= raw_agents_xy_copy_ptr[nda].y;
						float			ax			= (((float)col * cell_width) + adx) - fhsw;
						float			az			= (((float)row * cell_width) + adz) - fhsw;
						raw_agents_pos_ptr[aid + 0]	= ax;
						raw_agents_pos_ptr[aid + 2] = az;

						if( cell_id != raw_goal_cells_copy_ptr[nda] )
						{
							occupancy++;
						}
						offset++;
					}
				}
			}
		}
		thrust::get<1>(t) = occupancy;
	}
};
//
//=======================================================================================
//
void advance_agents(	thrust::device_vector<unsigned int>&	cells_ids,
						thrust::device_vector<unsigned int>&	occupancy,
						thrust::device_vector<unsigned int>&	agents_ids,
						thrust::device_vector<unsigned int>&	agents_ids_copy,
						thrust::device_vector<unsigned int>&	agents_goal_cell,
						thrust::device_vector<unsigned int>&	agents_goal_cell_copy,
						thrust::device_vector<unsigned int>&	agents_virt_cell,
						thrust::device_vector<unsigned int>&	agents_virt_cell_copy,
						thrust::device_vector<float2>&			agents_xy,
						thrust::device_vector<float2>&			agents_xy_copy,
						thrust::device_vector<float>&			agents_pos,
						unsigned int							scene_width_in_cells,
						float									cell_width				)
{
	unsigned int total_cells = scene_width_in_cells * scene_width_in_cells;
	thrust::copy( agents_ids.begin(), agents_ids.end(), agents_ids_copy.begin() );
	thrust::fill( agents_ids.begin(), agents_ids.end(), total_cells * DIRECTIONS );
	thrust::copy( agents_goal_cell.begin(), agents_goal_cell.end(), agents_goal_cell_copy.begin() );
	thrust::fill( agents_goal_cell.begin(), agents_goal_cell.end(), total_cells * DIRECTIONS );
	thrust::copy( agents_virt_cell.begin(), agents_virt_cell.end(), agents_virt_cell_copy.begin() );
	thrust::fill( agents_virt_cell.begin(), agents_virt_cell.end(), total_cells * DIRECTIONS );
	thrust::copy( agents_xy.begin(), agents_xy.end(), agents_xy_copy.begin() );
	thrust::fill( agents_xy.begin(), agents_xy.end(), make_float2( 0.0f, 0.0f ) );
	unsigned int*	raw_agents_ids_ptr		= thrust::raw_pointer_cast( agents_ids.data()				);
	unsigned int*	raw_agents_ids_copy_ptr	= thrust::raw_pointer_cast( agents_ids_copy.data()			);
	unsigned int*	raw_goal_cells_ptr		= thrust::raw_pointer_cast( agents_goal_cell.data()			);
	unsigned int*	raw_goal_cells_copy_ptr	= thrust::raw_pointer_cast( agents_goal_cell_copy.data()	);
	unsigned int*	raw_virt_cells_ptr		= thrust::raw_pointer_cast( agents_virt_cell.data()			);
	unsigned int*	raw_virt_cells_copy_ptr	= thrust::raw_pointer_cast( agents_virt_cell_copy.data()	);
	float2*			raw_agents_xy_ptr		= thrust::raw_pointer_cast( agents_xy.data()				);
	float2*			raw_agents_xy_copy_ptr	= thrust::raw_pointer_cast( agents_xy_copy.data()			);
	float*			raw_agents_pos_ptr		= thrust::raw_pointer_cast( agents_pos.data()				);
	thrust::for_each(
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_ids.begin(),
				occupancy.begin()
			)
		),
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_ids.end(),
				occupancy.end()
			)
		),
		advance_n_agents(	cell_width,
							scene_width_in_cells, 
							raw_agents_ids_ptr,
							raw_agents_ids_copy_ptr,
							raw_goal_cells_ptr,
							raw_goal_cells_copy_ptr,
							raw_virt_cells_ptr,
							raw_virt_cells_copy_ptr,
							raw_agents_xy_ptr,
							raw_agents_xy_copy_ptr,
							raw_agents_pos_ptr		)
	);
}
//
//=======================================================================================
//
struct advance_n_agents_virtually
{
	unsigned int	scene_width_in_cells;
	unsigned int	total_cells;
	unsigned int	tcd;
	float			f_deg_inc;
	unsigned int*	raw_agents_ids_ptr;
	unsigned int*	raw_agents_ids_copy_ptr;
	unsigned int*	raw_virtual_cells_ptr;
	unsigned int*	raw_goal_cells_ptr;

	advance_n_agents_virtually(	unsigned int	_swic,
								unsigned int*	_raip,
								unsigned int*	_raicp,
								unsigned int*	_rvcp,
								unsigned int*	_rgcp	) : scene_width_in_cells	(	_swic	),
															raw_agents_ids_ptr		(	_raip	),
															raw_agents_ids_copy_ptr	(	_raicp	),
															raw_virtual_cells_ptr	(	_rvcp	),
															raw_goal_cells_ptr		(	_rgcp	)
	{
		total_cells = scene_width_in_cells * scene_width_in_cells;
		tcd			= total_cells * DIRECTIONS;
		f_deg_inc	= 360.0f / (float)DIRECTIONS;
	}

	template <typename Tuple>						//0        1
	__host__ __device__ void operator()( Tuple t )	//CELL_ID, OCCUPANCY
	{
		unsigned int	cell_id				= thrust::get<0>(t);
		unsigned int	neighbor_cell_id	= total_cells;
		unsigned int	offset				= 0;
		unsigned int	occupancy			= 0;
		unsigned int	cda					= 0;
		unsigned int	nda					= 0;
		unsigned int	cdo					= 0;
		unsigned int	d					= 0;
		unsigned int	a					= 0;
		int				row					= (int)(cell_id / scene_width_in_cells);
		int				col					= (int)(cell_id % scene_width_in_cells);
		int				test_neighbor_col	= -1;
		int				test_neighbor_row	= -1;
		int				swic				= (int)scene_width_in_cells;
		float			test_radians		= 0.0f;

		for( a = 0; a < DIRECTIONS; a++ )	//STAYING
		{
			cda = cell_id * DIRECTIONS + a;
			if( raw_virtual_cells_ptr[cda] == tcd )
			{
				break;
			}
			if( raw_virtual_cells_ptr[cda] == cell_id )
			{
				cdo = cell_id * DIRECTIONS + offset;
				raw_agents_ids_ptr[cdo] = raw_agents_ids_copy_ptr[cda];
				if( cell_id != raw_goal_cells_ptr[cda] )
				{
					occupancy++;
				}
				offset++;
			}
		}

		for( d = 0; d < DIRECTIONS; d++ )	//INCOMING
		{
			test_radians		= DEG2RAD * ((float)d * f_deg_inc);
			test_neighbor_col	= col + ((int)SIGNF(  cosf( test_radians ) ));
			test_neighbor_row	= row + ((int)SIGNF( -sinf( test_radians ) ));
			if( INBOUNDS( test_neighbor_col, swic ) &&
				INBOUNDS( test_neighbor_row, swic )		)
			{
				neighbor_cell_id = (unsigned int)(test_neighbor_row * swic + test_neighbor_col);
				for( a = 0; a < DIRECTIONS; a++ )
				{
					nda = neighbor_cell_id * DIRECTIONS + a;
					if( raw_virtual_cells_ptr[nda] == tcd )
					{
						break;
					}
					if( raw_virtual_cells_ptr[nda] == cell_id )
					{
						cdo						= cell_id * DIRECTIONS + offset;
						raw_agents_ids_ptr[cdo] = raw_agents_ids_copy_ptr[nda];
						if( cell_id != raw_goal_cells_ptr[nda] )
						{
							occupancy++;
						}
						offset++;
					}
				}
			}
		}
		thrust::get<1>(t) = occupancy;
	}
};
//
//=======================================================================================
//
void advance_agents_virtually(	thrust::device_vector<unsigned int>&	cells_ids,
								thrust::device_vector<unsigned int>&	occupancy,
								thrust::device_vector<unsigned int>&	agents_ids,
								thrust::device_vector<unsigned int>&	agents_temp_ids,
								thrust::device_vector<unsigned int>&	agents_virt_cell,
								thrust::device_vector<unsigned int>&	agents_goal_cell,
								unsigned int							scene_width_in_cells	)

{
	unsigned int total_cells = scene_width_in_cells * scene_width_in_cells;
	thrust::copy( agents_ids.begin(), agents_ids.end(), agents_temp_ids.begin() );
	thrust::fill( agents_ids.begin(), agents_ids.end(), total_cells * DIRECTIONS );
	unsigned int* raw_agents_ids_ptr		= thrust::raw_pointer_cast( agents_ids.data()		);
	unsigned int* raw_agents_ids_copy_ptr	= thrust::raw_pointer_cast( agents_temp_ids.data()	);
	unsigned int* raw_virtual_cells_ptr		= thrust::raw_pointer_cast( agents_virt_cell.data()	);
	unsigned int* raw_goal_cells_ptr		= thrust::raw_pointer_cast( agents_goal_cell.data()	);
	thrust::for_each(
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_ids.begin(),
				occupancy.begin()
			)
		),
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_ids.end(),
				occupancy.end()
			)
		),
		advance_n_agents_virtually(	scene_width_in_cells, 
									raw_agents_ids_ptr,
									raw_agents_ids_copy_ptr,
									raw_virtual_cells_ptr,
									raw_goal_cells_ptr		)
	);
}
//
//=======================================================================================
//
struct fix_agent_collisions
{
	float			f_directions;
	float			f_deg_inc;
	int				swic;
	unsigned int	scene_width_in_cells;
	unsigned int	total_cells;
	unsigned int	num_agents;
	unsigned int*	raw_occ_ptr;
	unsigned int*	raw_future_occ_ptr;
	unsigned int*	raw_agents_future_ids_ptr;

	fix_agent_collisions(	unsigned int	_swic,
							unsigned int*	_rop,
							unsigned int*	_rfop,
							unsigned int*	_rafip	) : scene_width_in_cells		( _swic		),
														raw_occ_ptr					( _rop		),
														raw_future_occ_ptr			( _rfop		),
														raw_agents_future_ids_ptr	( _rafip	)
	{
		total_cells		= scene_width_in_cells * scene_width_in_cells;
		num_agents		= total_cells * DIRECTIONS;
		swic			= (int)scene_width_in_cells;
		f_directions	= (float)DIRECTIONS;
		f_deg_inc		= 360.0f / f_directions;
	}

	template <typename Tuple>								// 0         1         2                3                4    5
	inline __host__ __device__ void operator()(	Tuple t	)	// NCELL_ID, AGENT_ID, AGENT_GOAL_CELL, AGENT_VIRT_CELL, UID, ISEED
	{
		unsigned int	curr_cell			= thrust::get<0>(t);
		unsigned int	agent_id			= thrust::get<1>(t);
		unsigned int	goal_cell			= thrust::get<2>(t);
		unsigned int	next_cell			= thrust::get<3>(t);
		unsigned int	uid					= thrust::get<4>(t);
		int				seed				= thrust::get<5>(t);

		unsigned int	offset				= uid % DIRECTIONS;
		unsigned int	future_id1_index	= next_cell * DIRECTIONS + 1;

		int				i_row;
		int				i_col;
		int				i_next_row;
		int				i_next_col;
		float			f_diff_col;
		float			f_diff_row;
		float			f_curr_dir;

		float			dir_part;
		float			sig_part;
		float			num_part;
		float			f_mod_d02;
		float			f_mod_d12;
		float			f_alt_dir;

		float			d;
		float			test_radians;
		int				test_next_col;
		int				test_next_row;
		unsigned int	test_next_cell;


		if( agent_id == num_agents )
		{
			thrust::get<3>(t) = num_agents;
			return;
		}
		else
		{
																// STAY IN CELL WHEN:
			if( raw_occ_ptr[curr_cell]			>	0	&&		// MORE THAN ONE AGENT IN THE CELL.
				offset							==	0	&&		// FIRST AGENT IN THE CELL.
				raw_future_occ_ptr[curr_cell]	==	0		)	// CELL WILL NOT BE OCCUPIED NEXT.
			{
				thrust::get<3>(t) = curr_cell;
				return;
			}
																					// SEARCH FOR AN ALTERNATIVE WHEN:
			if( next_cell									!=	goal_cell	&&		//	DON'T AVOID GOAL
				raw_future_occ_ptr[next_cell]				>	0			&&		//	THERE'S A COLLISION			
				raw_agents_future_ids_ptr[future_id1_index] !=	agent_id		)	//	NOT THE FIRST AGENT IN CELL
			{

				i_row				= (int)(curr_cell / scene_width_in_cells);
				i_col				= (int)(curr_cell % scene_width_in_cells);
				i_next_row			= (int)(next_cell / scene_width_in_cells);
				i_next_col			= (int)(next_cell % scene_width_in_cells);
				f_diff_row			= (float)(i_next_row - i_row);
				f_diff_col			= (float)(i_next_col - i_col);
				f_curr_dir			= (RAD2DEG * atan2f( f_diff_row, f_diff_col )) / f_deg_inc;
				f_curr_dir			= fmod( f_directions - f_curr_dir, f_directions );

				for( d = 0; d < (f_directions - 1.0f); d++ )
				{
					if( raw_occ_ptr[curr_cell] > 1 && d < (float)offset ) continue;

					f_mod_d02		= fmod( d + 0.0f, 2.0f );
					f_mod_d12		= fmod( d + 1.0f, 2.0f );
					
					if( raw_occ_ptr[curr_cell] > 1 || seed % 2 )
					{
						dir_part	= f_directions * f_mod_d12;
						sig_part	= 1.0f - 2.0f * f_mod_d12;
					}
					else
					{
						dir_part	= f_directions * f_mod_d02;
						sig_part	= 1.0f - 2.0f * f_mod_d02;
					}
					
					num_part		= (int)d / 2 + 1.0f;
					f_alt_dir		= fmod( f_curr_dir + dir_part + sig_part * num_part, f_directions );
					test_radians	= DEG2RAD * f_alt_dir * f_deg_inc;

					test_next_col = i_col + ((int)SIGNF(  cosf( test_radians ) ));
					test_next_row = i_row + ((int)SIGNF( -sinf( test_radians ) ));
					if(	INBOUNDS( test_next_col, swic ) &&
						INBOUNDS( test_next_row, swic )		)
					{
						test_next_cell = (unsigned int)(test_next_row * swic + test_next_col);
						if( raw_future_occ_ptr[test_next_cell] == 0 )
						{
							thrust::get<3>(t) = test_next_cell;
							return;
						}
					}
				}
				thrust::get<3>(t) = curr_cell;	// NO ALTERNATIVE FOUND. STAY.
				return;
			}
		}
	}
};
//
//=======================================================================================
//
void fix_collisions(	thrust::device_vector<int>&				seeds,
						thrust::device_vector<unsigned int>&	occupancy,
						thrust::device_vector<unsigned int>&	future_occupancy,
						thrust::device_vector<unsigned int>&	agents_future_ids,
						thrust::device_vector<unsigned int>&	d_cells_ids,
						thrust::device_vector<unsigned int>&	agents_ids,
						thrust::device_vector<unsigned int>&	agents_dids,
						thrust::device_vector<unsigned int>&	agents_goal_cell,
						thrust::device_vector<unsigned int>&	agents_virt_cell,
						unsigned int							scene_width_in_cells,
						unsigned int							num_agents			)
{
	unsigned int* raw_occ_ptr				= thrust::raw_pointer_cast( occupancy.data()			);
	unsigned int* raw_future_occ_ptr		= thrust::raw_pointer_cast( future_occupancy.data()		);
	unsigned int* raw_agents_future_ids_ptr = thrust::raw_pointer_cast( agents_future_ids.data()	);
	// DCELL_ID, AGENT_ID, AGENT_GOAL_CELL, AGENT_VIRT_CELL, ISEED
	thrust::for_each(
		thrust::make_zip_iterator( 
			thrust::make_tuple(
				d_cells_ids.begin(),
				agents_ids.begin(),
				agents_goal_cell.begin(),
				agents_virt_cell.begin(),
				agents_dids.begin(),
				seeds.begin()
			) 
		), 
		thrust::make_zip_iterator( 
			thrust::make_tuple(
				d_cells_ids.end(),
				agents_ids.end(),
				agents_goal_cell.end(),
				agents_virt_cell.end(),
				agents_dids.end(),
				seeds.end()
			)
		),
		fix_agent_collisions(	scene_width_in_cells, 
								raw_occ_ptr,
								raw_future_occ_ptr,
								raw_agents_future_ids_ptr	)
	);
}
//
//=======================================================================================
//
struct populate_n_agents_rnd
{
	unsigned int	scene_width_in_cells;
	unsigned int*	raw_occ_ptr;
	float			cell_width;
	float			f_directions;
	float			f_deg_inc;
	int				swic;

	populate_n_agents_rnd(	float			_cell_width,
							unsigned int	_swic,
							unsigned int*	_rop		) : cell_width			( _cell_width	),
															scene_width_in_cells( _swic			),
															raw_occ_ptr			( _rop			)
	{
		f_directions	= (float)DIRECTIONS;
		f_deg_inc		= 360.0f / (float)DIRECTIONS;
		swic			= (int)scene_width_in_cells;
	}

	template <typename Tuple>								// 0         1         2                3                4         5             6             7    8
	inline __host__ __device__ void operator()(	Tuple t	)	// NCELL_ID, AGENT_ID, AGENT_GOAL_CELL, AGENT_VIRT_CELL, AGENT_XY, LOW_AGENT_ID, UPP_AGENT_ID, UID, ISEED
	{
		unsigned int cell_id			= thrust::get<0>(t);
		unsigned int uid				= thrust::get<7>(t);
		int seed						= thrust::get<8>(t);
		unsigned int total_cells		= scene_width_in_cells * scene_width_in_cells;
		thrust::default_random_engine rng( hash( seed ) );
		thrust::random::uniform_real_distribution<float> rDist( 0.0f, 1.0f );
		//thrust::random::uniform_int_distribution<unsigned int> uiDist( 0, total_cells );
		thrust::random::uniform_int_distribution<unsigned int> uiDist( 0, scene_width_in_cells - 1 );

		unsigned int low_aid			= thrust::get<5>(t);
		unsigned int upp_aid			= thrust::get<6>(t);
		unsigned int row				= cell_id / scene_width_in_cells;
		unsigned int col				= cell_id % scene_width_in_cells;
		unsigned int offset				= uid % DIRECTIONS;

		if( low_aid + offset < upp_aid )
		{
			//float			hWidth		= (float)scene_width_in_cells * cell_width / 2.0f;
			float			t_x			= cell_width * (float)col;
			float			t_y			= cell_width * (float)row;
			//float			a_x			= (t_x + cell_width * rDist( rng )) - hWidth;
			//float			a_y			= (t_y + cell_width * rDist( rng )) - hWidth;
			float			a_x			= cell_width * rDist( rng );
			float			a_y			= cell_width * rDist( rng );

			//unsigned int	gi			= uiDist( rng );
			//int			gx			= (int)(gi % scene_width_in_cells);
			//int			gy			= (int)(gi / scene_width_in_cells);
			int				gx			= (int)scene_width_in_cells - 1;
			int				gy			= (int)uiDist( rng );
			unsigned int	gi			= (unsigned int)(gy * (int)scene_width_in_cells + gx);
			int				diffX		= (gx - (int)t_x);
			int				ncx			= diffX;
			if( ncx != 0 )	ncx			= diffX / abs( diffX );
			int				diffY		= (gy - (int)t_y);
			int				ncy			= diffY;
			if( ncy != 0 )	ncy			= diffY / abs( diffY );
			unsigned int	ntx			= (unsigned int)(t_x + (float)ncx);
			unsigned int	nty			= (unsigned int)(t_y + (float)ncy);
			unsigned int	ni			= nty * scene_width_in_cells + ntx;
			
			thrust::get<1>(t)			= low_aid + offset;			//AGENT_ID
			thrust::get<2>(t)			= gi;						//GOAL
			thrust::get<3>(t)			= ni;						//VIRTUAL
			thrust::get<4>(t)			= make_float2( a_x, a_y );
		}
		else
		{
			thrust::get<1>(t)			= total_cells * DIRECTIONS;	//AGENT_ID
			thrust::get<2>(t)			= total_cells;				//GOAL
			thrust::get<3>(t)			= total_cells;				//VIRTUAL
			thrust::get<4>(t)			= make_float2( 0.0f, 0.0f );
		}
	}
};
//
//=======================================================================================
//
struct populate_n_agents
{
	unsigned int	scene_width_in_cells;
	unsigned int*	raw_occ_ptr;
	float			cell_width;
	float			f_directions;
	float			f_deg_inc;
	float			fhsw;
	int				swic;
	float*			raw_pos_ptr;
	float*			raw_goal_ptr;

	populate_n_agents(	float			_cell_width,
						unsigned int	_swic,
						unsigned int*	_rop,
						float*			_rpp,
						float*			_rgp		) : cell_width			( _cell_width	),
														scene_width_in_cells( _swic			),
														raw_occ_ptr			( _rop			),
														raw_pos_ptr			( _rpp			),
														raw_goal_ptr		( _rgp			)
	{
		f_directions	= (float)DIRECTIONS;
		f_deg_inc		= 360.0f / (float)DIRECTIONS;
		swic			= (int)scene_width_in_cells;
		fhsw			= ((float)scene_width_in_cells * cell_width) / 2.0f;
	}

	template <typename Tuple>								// 0         1         2                3                4         5             6             7
	inline __host__ __device__ void operator()(	Tuple t	)	// NCELL_ID, AGENT_ID, AGENT_GOAL_CELL, AGENT_VIRT_CELL, AGENT_XY, LOW_AGENT_ID, UPP_AGENT_ID, UID
	{
		unsigned int	cell_id			= thrust::get<0>(t);
		unsigned int	uid				= thrust::get<7>(t);
		unsigned int	total_cells		= scene_width_in_cells * scene_width_in_cells;

		unsigned int	low_aid			= thrust::get<5>(t);
		unsigned int	upp_aid			= thrust::get<6>(t);
		unsigned int	row				= cell_id / scene_width_in_cells;
		unsigned int	col				= cell_id % scene_width_in_cells;
		unsigned int	offset			= uid % DIRECTIONS;

		unsigned int	index			= (low_aid + offset) * 4;
		float			agent_pos_x		= raw_pos_ptr[index+0]  + fhsw;
		float			agent_pos_z		= raw_pos_ptr[index+2]  + fhsw;
		float			agent_goal_x	= raw_goal_ptr[index+0] + fhsw;
		float			agent_goal_z	= raw_goal_ptr[index+2] + fhsw;

		if( low_aid + offset < upp_aid )
		{
			float			t_x			= cell_width * (float)col;
			float			t_y			= cell_width * (float)row;
			float			a_x			= fmod( agent_pos_x, cell_width );
			float			a_y			= fmod( agent_pos_z, cell_width );

			int				gx			= (int)(agent_goal_x / cell_width);
			int				gy			= (int)(agent_goal_z / cell_width);

			unsigned int	gi			= (unsigned int)(gy * (int)scene_width_in_cells + gx);
			int				diffX		= (gx - (int)t_x);
			int				ncx			= diffX;
			if( ncx != 0 )	ncx			= diffX / abs( diffX );
			int				diffY		= (gy - (int)t_y);
			int				ncy			= diffY;
			if( ncy != 0 )	ncy			= diffY / abs( diffY );
			unsigned int	ntx			= (unsigned int)((int)col + ncx);
			unsigned int	nty			= (unsigned int)((int)row + ncy);
			unsigned int	ni			= nty * scene_width_in_cells + ntx;
			
			thrust::get<1>(t)			= low_aid + offset;			//AGENT_ID
			thrust::get<2>(t)			= gi;						//GOAL
			thrust::get<3>(t)			= ni;						//VIRTUAL
			thrust::get<4>(t)			= make_float2( a_x, a_y );
		}
		else
		{
			thrust::get<1>(t)			= total_cells * DIRECTIONS;	//AGENT_ID
			thrust::get<2>(t)			= total_cells;				//GOAL
			thrust::get<3>(t)			= total_cells;				//VIRTUAL
			thrust::get<4>(t)			= make_float2( 0.0f, 0.0f );
		}
	}
};
//
//=======================================================================================
//
struct fill_cell_id_rnd
{
	float			scale;
	float			cell_width;
	unsigned int	scene_width_in_cells;

	fill_cell_id_rnd(	float			_scale,
						float			_cell_width,
						unsigned int	_swic	) : scale					(	_scale		),
													cell_width				(	_cell_width	),
													scene_width_in_cells	(	_swic		)
	{

	}

	template <typename Tuple>								// 0		1		  2			3		  4
	inline __host__ __device__ void operator()( Tuple t )	// CELL_ID, FRANDOM1, FRANDOM2, FRANDOM3, FRANDOM4
	{
		float x = scale * thrust::get<1>(t);
		float y = scale * thrust::get<2>(t);
		float s = thrust::get<3>(t);
		if( s < 0.5 )
		{
			x = -x;
		}
		s = thrust::get<4>(t);
		if( s < 0.5 )
		{
			y = -y;
		}
		x += scale;
		y += scale;

		unsigned int tx = (unsigned int)(x / cell_width);
		unsigned int ty = (unsigned int)(y / cell_width);
		unsigned int tile = ty * scene_width_in_cells + tx;
		thrust::get<0>(t) = tile;
	}
};
//
//=======================================================================================
//
struct fill_cell_id
{
	float			scale;
	float			cell_width;
	unsigned int	scene_width_in_cells;
	float*			raw_pos_ptr;
	float			half_scene_width;

	fill_cell_id(	float			_scale,
					float			_cell_width,
					unsigned int	_swic,
					float*			_rpp)		:	scale					(	_scale		),
													cell_width				(	_cell_width	),
													scene_width_in_cells	(	_swic		),
													raw_pos_ptr				(	_rpp		)
	{
		half_scene_width =  ((float)scene_width_in_cells * cell_width) / 2.0f;
	}

	template <typename Tuple>								// 0		1
	inline __host__ __device__ void operator()( Tuple t )	// CELL_ID,	UID
	{
		unsigned int index = thrust::get<1>(t) * 4;
		float x = raw_pos_ptr[index+0] + half_scene_width;
		float z = raw_pos_ptr[index+2] + half_scene_width;

		unsigned int tx = (unsigned int)(x / cell_width);
		unsigned int tz = (unsigned int)(z / cell_width);
		unsigned int tile = tz * scene_width_in_cells + tx;
		thrust::get<0>(t) = tile;
	}
};
//
//=======================================================================================
//
struct gen_randomf
{
	unsigned int skip;

	gen_randomf( unsigned int _skip ) : skip( _skip )
	{

	}

	template <typename Tuple>						// 0        1
	__host__ __device__ void operator()( Tuple t )	// FRESULT, ISEED
	{
		thrust::default_random_engine rng( hash( thrust::get<1>(t) ) );
		rng.discard( skip );
		thrust::random::uniform_real_distribution<float> rDist;
		thrust::get<0>(t) = rDist( rng );
	}
};
//
//=======================================================================================
//
struct ui_minus_one
{
	template <typename Tuple>
	__host__ __device__ void operator()( Tuple t )
	{
		if( (unsigned int)thrust::get<0>(t) > 0 )
		{
			thrust::get<0>(t) = thrust::get<0>(t) - 1;
		}
	}
};
//
//=======================================================================================
//
void generate_cells_and_agents(	thrust::device_vector<int>&				seeds,
								thrust::device_vector<unsigned int>&	cells_ids,
								thrust::device_vector<unsigned int>&	d_cells_ids,
								thrust::device_vector<unsigned int>&	cells_occ,
								thrust::device_vector<unsigned int>&	agents_ids,
								thrust::device_vector<unsigned int>&	agents_dids,
								thrust::device_vector<unsigned int>&	agents_goal_cell,
								thrust::device_vector<unsigned int>&	agents_virt_cell,
								thrust::device_vector<float2>&			agents_xy,
								float									scale,
								float									cell_width,
								unsigned int							scene_width_in_cells,
								unsigned int							num_agents			)
{
	unsigned int total_cells = scene_width_in_cells * scene_width_in_cells;

	thrust::sequence( cells_ids.begin(), cells_ids.end() );
	repeated_range<UI_Iterator>	n_cells_ids	( cells_ids.begin(), cells_ids.end(), DIRECTIONS );
	thrust::copy( n_cells_ids.begin(), n_cells_ids.end(), d_cells_ids.begin() );

	thrust::device_vector<unsigned int> low_aids( total_cells );
	thrust::device_vector<unsigned int> upp_aids( total_cells );
	thrust::device_vector<unsigned int> agent_cell_ids( num_agents );

	thrust::counting_iterator<unsigned int> aids_begin( 0 );
	thrust::counting_iterator<unsigned int> aids_end = aids_begin + num_agents;
	thrust::device_vector<float> r0( num_agents );
	thrust::device_vector<float> r1( num_agents );
	thrust::device_vector<float> r2( num_agents );
	thrust::device_vector<float> r3( num_agents );
	thrust::for_each(	thrust::make_zip_iterator( thrust::make_tuple( r0.begin(), seeds.begin() ) ), 
						thrust::make_zip_iterator( thrust::make_tuple( r0.end(), seeds.begin() + r0.size() ) ), 
						gen_randomf( 0 * num_agents ) );
	thrust::for_each(	thrust::make_zip_iterator( thrust::make_tuple( r1.begin(), seeds.begin() ) ), 
						thrust::make_zip_iterator( thrust::make_tuple( r1.end(), seeds.begin() + r1.size() ) ), 
						gen_randomf( 1 * num_agents ) );
	thrust::for_each(	thrust::make_zip_iterator( thrust::make_tuple( r2.begin(), seeds.begin() ) ), 
						thrust::make_zip_iterator( thrust::make_tuple( r2.end(), seeds.begin() + r2.size() ) ), 
						gen_randomf( 2 * num_agents ) );
	thrust::for_each(	thrust::make_zip_iterator( thrust::make_tuple( r3.begin(), seeds.begin() ) ), 
						thrust::make_zip_iterator( thrust::make_tuple( r3.end(), seeds.begin() + r3.size() ) ), 
						gen_randomf( 3 * num_agents ) );

	thrust::for_each(	
		thrust::make_zip_iterator(
			thrust::make_tuple(
				agent_cell_ids.begin(),
				r0.begin(),
				r1.begin(),
				r2.begin(),
				r3.begin()
			)
		),
		thrust::make_zip_iterator(
			thrust::make_tuple(
				agent_cell_ids.end(),
				r0.end(),
				r1.end(),
				r2.end(),
				r3.end()
			)
		),
		fill_cell_id_rnd(	scale, 
							cell_width, 
							scene_width_in_cells )
	);
	thrust::device_vector<unsigned int> complement( total_cells );
	thrust::sequence( complement.begin(), complement.end() );
	thrust::device_vector<unsigned int> agent_cell_ids2( num_agents + total_cells );
	thrust::copy( agent_cell_ids.begin(), agent_cell_ids.end(), agent_cell_ids2.begin() );
	thrust::copy( complement.begin(), complement.end(), agent_cell_ids2.begin() + num_agents );
	dense_histogram( agent_cell_ids2, cells_occ );
	thrust::for_each(
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_occ.begin()
			)
		), 
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_occ.end()
			)
		),
		ui_minus_one()
	);
	thrust::inclusive_scan( cells_occ.begin(), cells_occ.end(), upp_aids.begin() );
	thrust::transform( upp_aids.begin(), upp_aids.end(), cells_occ.begin(), low_aids.begin(), minus_ui );
	
	// DCELL_ID, AGENT_ID, AGENT_GOAL_CELL, AGENT_VIRT_CELL, AGENT_X, AGENT_Y, NLOW_AGENT_ID, NUPP_AGENT_ID, UID
	repeated_range<UI_Iterator> n_low_aids( low_aids.begin(), low_aids.end(), DIRECTIONS );
	repeated_range<UI_Iterator> n_upp_aids( upp_aids.begin(), low_aids.end(), DIRECTIONS );
	thrust::sequence( agents_dids.begin(), agents_dids.end() );
	unsigned int* raw_occ_ptr = thrust::raw_pointer_cast( cells_occ.data() );

	thrust::for_each(
		thrust::make_zip_iterator( 
			thrust::make_tuple(
				d_cells_ids.begin(),
				agents_ids.begin(),
				agents_goal_cell.begin(),
				agents_virt_cell.begin(),
				agents_xy.begin(),
				n_low_aids.begin(),
				n_upp_aids.begin(),
				agents_dids.begin(),
				seeds.begin()
			)
		),
		thrust::make_zip_iterator( 
			thrust::make_tuple(
				d_cells_ids.end(),
				agents_ids.end(),
				agents_goal_cell.end(),
				agents_virt_cell.end(),
				agents_xy.end(),
				n_low_aids.end(),
				n_upp_aids.end(),
				agents_dids.end(),
				seeds.end()
			)
		),
		populate_n_agents_rnd( cell_width, scene_width_in_cells, raw_occ_ptr )
	);
}
//
//=======================================================================================
//
void init_cells_and_agents	(	thrust::device_vector<float>&			agents_pos,
								thrust::device_vector<float>&			agents_goal,
								thrust::device_vector<float>&			lca,
								thrust::device_vector<int>&				seeds,
								thrust::device_vector<unsigned int>&	cells_ids,
								thrust::device_vector<unsigned int>&	d_cells_ids,
								thrust::device_vector<unsigned int>&	cells_occ,
								thrust::device_vector<unsigned int>&	agents_ids,
								thrust::device_vector<unsigned int>&	agents_dids,
								thrust::device_vector<unsigned int>&	agents_goal_cell,
								thrust::device_vector<unsigned int>&	agents_virt_cell,
								thrust::device_vector<float2>&			agents_xy,
								float									scale,
								float									cell_width,
								unsigned int							scene_width_in_cells,
								unsigned int							num_agents			)
{
	unsigned int total_cells	= scene_width_in_cells * scene_width_in_cells;

	thrust::sequence( cells_ids.begin(), cells_ids.end() );
	repeated_range<UI_Iterator>	n_cells_ids	( cells_ids.begin(), cells_ids.end(), DIRECTIONS );
	thrust::copy( n_cells_ids.begin(), n_cells_ids.end(), d_cells_ids.begin() );

	thrust::device_vector<unsigned int> low_aids( total_cells );
	thrust::device_vector<unsigned int> upp_aids( total_cells );
	thrust::device_vector<unsigned int> agent_cell_ids( num_agents );

	thrust::counting_iterator<unsigned int> aids_begin( 0 );
	thrust::counting_iterator<unsigned int> aids_end = aids_begin + num_agents;

	float* raw_pos_ptr			= thrust::raw_pointer_cast( agents_pos.data()	);
	float* raw_goal_ptr			= thrust::raw_pointer_cast( agents_goal.data()	);

	thrust::for_each(	
		thrust::make_zip_iterator(
			thrust::make_tuple(
				agent_cell_ids.begin(),
				cells_ids.begin()
			)
		),
		thrust::make_zip_iterator(
			thrust::make_tuple(
				agent_cell_ids.end(),
				cells_ids.end()
			)
		),
		fill_cell_id(	scale,
						cell_width, 
						scene_width_in_cells,
						raw_pos_ptr			)
	);
	thrust::device_vector<unsigned int> complement( total_cells );
	thrust::sequence( complement.begin(), complement.end() );
	thrust::device_vector<unsigned int> agent_cell_ids2( num_agents + total_cells );
	thrust::copy( agent_cell_ids.begin(), agent_cell_ids.end(), agent_cell_ids2.begin() );
	thrust::copy( complement.begin(), complement.end(), agent_cell_ids2.begin() + num_agents );
	dense_histogram( agent_cell_ids2, cells_occ );
	thrust::for_each(
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_occ.begin()
			)
		), 
		thrust::make_zip_iterator(
			thrust::make_tuple(
				cells_occ.end()
			)
		),
		ui_minus_one()
	);
	thrust::inclusive_scan( cells_occ.begin(), cells_occ.end(), upp_aids.begin() );
	thrust::transform( upp_aids.begin(), upp_aids.end(), cells_occ.begin(), low_aids.begin(), minus_ui );
	
	// DCELL_ID, AGENT_ID, AGENT_GOAL_CELL, AGENT_VIRT_CELL, AGENT_X, AGENT_Y, NLOW_AGENT_ID, NUPP_AGENT_ID, UID
	repeated_range<UI_Iterator> n_low_aids( low_aids.begin(), low_aids.end(), DIRECTIONS );
	repeated_range<UI_Iterator> n_upp_aids( upp_aids.begin(), low_aids.end(), DIRECTIONS );
	thrust::sequence( agents_dids.begin(), agents_dids.end() );
	unsigned int* raw_occ_ptr = thrust::raw_pointer_cast( cells_occ.data() );

	thrust::for_each(
		thrust::make_zip_iterator( 
			thrust::make_tuple(
				d_cells_ids.begin(),
				agents_ids.begin(),
				agents_goal_cell.begin(),
				agents_virt_cell.begin(),
				agents_xy.begin(),
				n_low_aids.begin(),
				n_upp_aids.begin(),
				agents_dids.begin()
			)
		),
		thrust::make_zip_iterator( 
			thrust::make_tuple(
				d_cells_ids.end(),
				agents_ids.end(),
				agents_goal_cell.end(),
				agents_virt_cell.end(),
				agents_xy.end(),
				n_low_aids.end(),
				n_upp_aids.end(),
				agents_dids.end()
			)
		),
		populate_n_agents( cell_width, scene_width_in_cells, raw_occ_ptr, raw_pos_ptr, raw_goal_ptr )
	);
}
//
//=======================================================================================
//
unsigned int count_collisions( thrust::device_vector<unsigned int>& occupancy )
{
	unsigned int result = 0;
	thrust::device_vector<unsigned int> occ_cpy = occupancy;
	thrust::for_each(
		thrust::make_zip_iterator(
			thrust::make_tuple(
				occ_cpy.begin()
			)
		), 
		thrust::make_zip_iterator(
			thrust::make_tuple(
				occ_cpy.end()
			)
		),
		ui_minus_one()
	);
	result = thrust::reduce( occ_cpy.begin(), occ_cpy.end() );
	return result;
}
//
//=======================================================================================
